package com.teammerge.safetrip;

import android.content.Context;
import android.net.Uri;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.work.Data;
import androidx.work.WorkerParameters;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.*;


@RunWith(AndroidJUnit4.class)
public class TripEvalTest {
    private Context mContext;
    private Executor mExecutor;
    TripEvaluation tripEvaluation;
    ArrayBlockingQueue<String> tripFetchQueue = new ArrayBlockingQueue<>(1);
    int tripFetchedCounter = 0;


    @Before
    public void setUp() {
        mContext = ApplicationProvider.getApplicationContext();
        mExecutor = Executors.newSingleThreadExecutor();


        final String rootDir = mContext.getFilesDir().getAbsolutePath();
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference fileRef = storage.getReference().child("super-admin/test_case_2.txt");
        final File logFile = new File(rootDir + "/logData.txt");


        fileRef.getFile(logFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                BufferedReader reader = null;
                int lengthOfFile = 0;
                try {
                    reader = new BufferedReader(new FileReader(logFile));
                    while (reader.readLine() != null) lengthOfFile++;
                    reader.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Data data = new Data.Builder()
                        .putInt("LENGTH_OF_FILE", lengthOfFile)
                        .build();


                Uri file = Uri.fromFile(logFile);

                WorkerParameters workerParameters = new WorkerParameters(new UUID(5,47), data, new ArrayList<String>(),
                        null, 0, mExecutor,
                        null, null, null, null);

                tripEvaluation = new TripEvaluation(mContext, workerParameters, lengthOfFile, file);

                tripEvaluation.readData();
                tripEvaluation.trip.setStartTime(new Date(tripEvaluation.time[0]));
                tripEvaluation.trip.setEndTime(new Date(tripEvaluation.time[tripEvaluation.length-1]));

                tripFetchQueue.add("Done");
                tripFetchedCounter++;

            }

        });

        try {
            while(tripFetchQueue.take() != null){
                if(tripFetchedCounter >= 1){
                    break;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void calculateJerksScoreTest() {
        tripEvaluation.calculateJerks();
        tripEvaluation.calculatePCA();
        tripEvaluation.projectJerks();
        tripEvaluation.calculateJerkScore();

        assertEquals(100.0, tripEvaluation.trip.getJerkScore(), 0.1);
    }


    @Test
    public void contextViolationsClearAndDryTest() {
        Weather weather = new Weather();
        weather.setPrecipIntensity(0);
        weather.setTemperature(20.0);
        weather.setIndex(0);
        tripEvaluation.trip.getWeather().add(weather);

        tripEvaluation.analyzeContextViolations();
        tripEvaluation.calculateContextScore();

        assertEquals(100.0, tripEvaluation.trip.getContextScore(), 0.1);
    }

    @Test
    public void contextViolationsLightRainAndLightSnowTest() {
        Weather weather = new Weather();
        weather.setPrecipIntensity(0.5);
        weather.setPrecipProbability(0.5);
        weather.setTemperature(20.0);
        weather.setIndex(0);

        tripEvaluation.trip.getWeather().add(weather);

        tripEvaluation.analyzeContextViolations();
        tripEvaluation.calculateContextScore();

        assertEquals(52.105, tripEvaluation.trip.getContextScore(), 0.1);
    }

    @Test
    public void contextViolationsHeavyRainTest() {
        Weather weather = new Weather();
        weather.setPrecipIntensity(1.5);
        weather.setPrecipProbability(1);
        weather.setTemperature(4.0);
        weather.setIndex(0);

        tripEvaluation.trip.getWeather().add(weather);

        tripEvaluation.analyzeContextViolations();
        tripEvaluation.calculateContextScore();

        assertEquals(31.15, tripEvaluation.trip.getContextScore(), 0.1);
    }

    @Test
    public void contextViolationsHeavySnowTest() {
        Weather weather = new Weather();
        weather.setPrecipIntensity(1.5);
        weather.setPrecipProbability(1);
        weather.setTemperature(-1.0);
        weather.setIndex(0);

        tripEvaluation.trip.getWeather().add(weather);

        tripEvaluation.analyzeContextViolations();
        tripEvaluation.calculateContextScore();

        assertEquals(0.0, tripEvaluation.trip.getContextScore(), 0.1);
    }

    @Test
    public void analyzeSpeedErrorsTest() {
        tripEvaluation.analyzeSpeedErrors();
        tripEvaluation.calculateSpeedScore();

        assertEquals(71.79, tripEvaluation.trip.getSpeedScore(), 0.1);

    }

    @Test
    public void analyzeSpeedStatsTest() {
        tripEvaluation.analyzeSpeedStats();

        assertEquals(44.0, tripEvaluation.trip.getAverageSpeed(), 0.1);
        assertEquals(0.636, tripEvaluation.trip.getRelativeSpeedAverage(), 0.1);
    }

    @Test
    public void calculateDistractionNoPhoneUseScoreTest() {
        tripEvaluation.calculateDistractionScore();
        assertEquals(0.0, tripEvaluation.trip.getJerkScore(), 0.1);
    }

    @Test
    public void calculateDistractionWithPhoneUseScoreTest() {
        mContext = ApplicationProvider.getApplicationContext();
        mExecutor = Executors.newSingleThreadExecutor();


        final String rootDir = mContext.getFilesDir().getAbsolutePath();
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference fileRef = storage.getReference().child("super-admin/test_case_1.txt");
        final File logFile = new File(rootDir + "/logData.txt");


        fileRef.getFile(logFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                BufferedReader reader = null;
                int lengthOfFile = 0;
                try {
                    reader = new BufferedReader(new FileReader(logFile));
                    while (reader.readLine() != null) lengthOfFile++;
                    reader.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Data data = new Data.Builder()
                        .putInt("LENGTH_OF_FILE", lengthOfFile)
                        .build();


                Uri file = Uri.fromFile(logFile);

                WorkerParameters workerParameters = new WorkerParameters(new UUID(5,47), data, new ArrayList<String>(),
                        null, 0, mExecutor,
                        null, null, null, null);

                tripEvaluation = new TripEvaluation(mContext, workerParameters, lengthOfFile, file);

                tripEvaluation.readData();
                tripEvaluation.trip.setStartTime(new Date(tripEvaluation.time[0]));
                tripEvaluation.trip.setEndTime(new Date(tripEvaluation.time[tripEvaluation.length-1]));

                tripFetchQueue.add("Done");
                tripFetchedCounter++;

            }

        });

        try {
            while(tripFetchQueue.take() != null){
                if(tripFetchedCounter >= 1){
                    break;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }





        tripEvaluation.calculateDistractionScore();
        assertEquals(18.59, tripEvaluation.trip.getPercentageScreenOn(), 0.1);
        assertEquals(0.0, tripEvaluation.trip.getDistractionScore(), 0.1);
    }

    @Test
    public void calculateScoreTest() {
        tripEvaluation.calculateJerks();
        tripEvaluation.calculatePCA();
        tripEvaluation.projectJerks();
        tripEvaluation.calculateJerkScore();
        tripEvaluation.calculateDistractionScore();
        tripEvaluation.analyzeSpeedErrors();
        tripEvaluation.calculateSpeedScore();

        Weather weather = new Weather();
        weather.setPrecipIntensity(0);
        weather.setTemperature(20.0);
        weather.setIndex(0);
        tripEvaluation.trip.getWeather().add(weather);

        tripEvaluation.analyzeContextViolations();
        tripEvaluation.calculateContextScore();


        tripEvaluation.calculateScore();

        assertEquals(91.0, tripEvaluation.trip.getScore(), 0.1);
    }


    
}
