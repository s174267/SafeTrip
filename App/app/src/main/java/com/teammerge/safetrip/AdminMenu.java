package com.teammerge.safetrip;

import androidx.appcompat.app.AppCompatActivity;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class AdminMenu extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_menu);

        Button analyzeTripButton = findViewById(R.id.btnAnalyzeTrip);

        final String rootDir = this.getFilesDir().getAbsolutePath();

        analyzeTripButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference fileRef = storage.getReference().child("super-admin/logfile.txt");
                final File logFile = new File(rootDir + "/logData.txt");
                fileRef.getFile(logFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                        BufferedReader reader = null;
                        int lengthOfFile = 0;
                        try {
                            reader = new BufferedReader(new FileReader(logFile));
                            while (reader.readLine() != null) lengthOfFile++;
                            reader.close();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        Data data = new Data.Builder()
                                .putInt("LENGTH_OF_FILE", lengthOfFile)
                                .build();

                        Constraints constraints = new Constraints.Builder()
                                .setRequiredNetworkType(NetworkType.CONNECTED)
                                .build();

                        OneTimeWorkRequest tripWorker = new OneTimeWorkRequest.Builder(TripEvaluation.class)
                                .setConstraints(constraints)
                                .setInputData(data)
                                .build();

                        WorkManager.getInstance(getApplicationContext()).enqueue(tripWorker);
                    }
                });


            }
        });
    }
}
