package com.teammerge.safetrip;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AppDisplayUpdate extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        double speed = intent.getDoubleExtra("speed",0);
        double speedLimit = intent.getDoubleExtra("speedLimit",0);
        double lat = intent.getDoubleExtra("lat",0);
        double lng = intent.getDoubleExtra("lng",0);
        long time = intent.getLongExtra("time", 0);
        int lengthOfFile = intent.getIntExtra("lengthOfFile", 0);
        try {

            MainActivity.getInstance().updateDisplayText(String.valueOf(Math.round(speed*100.0)/100.0),
                                                        String.valueOf(Math.round(speedLimit*100.0)/100.0),
                                                        lat,
                                                        lng,
                                                        String.valueOf(time/1000),
                                                        lengthOfFile);
        } catch (Exception e) {

        }
    }

}
