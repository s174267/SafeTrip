package com.teammerge.safetrip;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class AuthenticationActivity extends Activity {

    private FirebaseAuth mAuth;
    private EditText EmailField;
    private EditText PasswordField;
    private Button btnCreateUser;
    private Button btnSignIn;
    private ProgressBar progressBar;

    private static final String TAG_LOGIN = "CurrentUser_Login";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);

        EmailField = findViewById(R.id.emailField);
        PasswordField = findViewById(R.id.paswordField);
        btnSignIn = findViewById(R.id.btn_signIn);
        btnCreateUser = findViewById(R.id.btn_createUser);
        progressBar = findViewById(R.id.progressBarSignIn);
        mAuth = FirebaseAuth.getInstance();

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn(EmailField.getText().toString(), PasswordField.getText().toString());
            }
        });

        btnCreateUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();

        FirebaseUser currentUser = mAuth.getCurrentUser();

        if (currentUser != null) {
            Log.d(TAG_LOGIN, "User is signed in");
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            Log.d(TAG_LOGIN, "No user is signed in");
        }
    }



    private void signIn(String email, String password) {
        Log.d(TAG_LOGIN, "signIn with email: " + email);

        if (!isValidateEmail(email)) {
            Toast.makeText(AuthenticationActivity.this, "Email address is not valid.",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        if (password == null || password.isEmpty()) {
            Toast.makeText(AuthenticationActivity.this, "Password is not valid.",
                    Toast.LENGTH_SHORT).show();
            return;
        }


        progressBar.setVisibility(View.VISIBLE);
        btnSignIn.setEnabled(false);
        btnCreateUser.setEnabled(false);


        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG_LOGIN, "signInWithEmail:success");
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                        } else {
                            Log.w(TAG_LOGIN, "signInWithEmail:failure", task.getException());
                            Toast.makeText(AuthenticationActivity.this, R.string.toast_AuthenticationFailed,
                                    Toast.LENGTH_SHORT).show();
                        }
                        progressBar.setVisibility(View.INVISIBLE);
                        btnSignIn.setEnabled(true);
                        btnCreateUser.setEnabled(true);

                    }
                });
    }

    private boolean isValidateEmail(String email) {
        String emailRegex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(emailRegex);
    }

}
