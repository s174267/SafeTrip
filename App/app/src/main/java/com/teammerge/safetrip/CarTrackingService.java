package com.teammerge.safetrip;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.here.android.mpa.common.ApplicationContext;
import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.MapEngine;
import com.here.android.mpa.common.MatchedGeoPosition;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.guidance.NavigationManager;
import com.here.android.mpa.prefetcher.MapDataPrefetcher;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.util.Date;

import androidx.core.app.NotificationCompat;

public class CarTrackingService extends Service {

    private static final int NOTIF_ID = 1;
    private static final String NOTIF_CHANNEL_ID = "42";

    boolean tripInProgress = false;

    private SensorManager sensorManager;
    private Sensor linAcc;
    private Sensor gyroSensor;
    private Sensor rotationSensor;

    private int lengthOfFile = 0;

    private BroadcastReceiver mScreenStateReceiver = new ScreenStateReceiver();


    private float xN = 0;
    private float yN = 0;
    private float zN = 0;

    private float x;
    private float y;
    private float z;

    private double xC;
    private double yC;
    private double zC;

    private long startTime = 0;
    private long currentTime = 0;

    private double speed;
    private double speedLimit;

    private double lat = 0;
    private double lng = 0;

    private float gyroX = 0;
    private float gyroY = 0;
    private float gyroZ = 0;

    private float rotX = 0;
    private float rotY = 0;
    private float rotZ = 0;
    private float rotScal = 0;

    private boolean fetchingDataInProgress = false;

    private long lastPositionUpdate = new Date().getTime();

    public CarTrackingService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        startService();
        startTracking();

        return super.onStartCommand(intent, flags, startId);
    }

    public void startService(){

        Intent notificationIntent = new Intent(this, MainActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        startForeground(NOTIF_ID, new NotificationCompat.Builder(this,
                NOTIF_CHANNEL_ID)
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.text_SafeTripIsMonitoringYourDrivingHabits))
                .setContentIntent(pendingIntent)
                .build());

    }

    public void startTracking(){
        startTime = new Date().getTime();
        lastPositionUpdate = new Date().getTime();
        boolean success = com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(getApplicationContext().getExternalFilesDir(null) + File.separator + ".here-maps", "com.teammerge.safetrip");

        if(true) {
            MapEngine mapEngine = MapEngine.getInstance();
            ApplicationContext appContext = new ApplicationContext(getApplicationContext());

            mapEngine.init(appContext, new OnEngineInitListener() {
                @Override
                public void onEngineInitializationCompleted(Error error) {
                    if (error == OnEngineInitListener.Error.NONE) {

                        MapEngine.getInstance().onResume();
                        startNavigationManager();
                        startPositioningManager();
                        PositioningManager.getInstance().addListener(new WeakReference<>(positionLister));
                        MapDataPrefetcher.getInstance().addListener(prefetcherListener);
                        Log.d("ENGINE INIT", "ENGINE HAS STARTED");
                    } else {
                        Log.d("ENGINE INIT", error.getDetails());

                    }
                }
            });
        }
        IntentFilter screenStateFilter = new IntentFilter();
        screenStateFilter.addAction(Intent.ACTION_USER_PRESENT);
        screenStateFilter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(mScreenStateReceiver, screenStateFilter);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        linAcc = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        gyroSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);

        sensorManager.registerListener(linAccSensorEvent, linAcc, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(gyroSensorEvent, gyroSensor, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(rotationSensorEvent, rotationSensor, SensorManager.SENSOR_DELAY_NORMAL);


    }

    private void startPositioningManager(){
        boolean positioningManagerStarted = PositioningManager.getInstance().start(PositioningManager.LocationMethod.GPS_NETWORK);

        if (!positioningManagerStarted) {
            Toast.makeText(this, R.string.toast_UnableToStartPositioningManager, Toast.LENGTH_LONG);
        }
    }
    private void startNavigationManager() {
        NavigationManager.Error navError = NavigationManager.getInstance().startTracking();
        NavigationManager.getInstance().setSpeedWarningEnabled(false);
        if (navError != NavigationManager.Error.NONE) {
            Toast.makeText(this, R.string.toast_UbableToStartNavigationManager, Toast.LENGTH_LONG);
        }
    }

    SensorEventListener rotationSensorEvent = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            rotX = sensorEvent.values[0];
            rotY = sensorEvent.values[1];
            rotZ = sensorEvent.values[2];
            rotScal = sensorEvent.values[3];

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };

    SensorEventListener gyroSensorEvent = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            gyroX = sensorEvent.values[0];
            gyroY = sensorEvent.values[1];
            gyroZ = sensorEvent.values[2];
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };

    SensorEventListener linAccSensorEvent = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            x = event.values[0];
            y = event.values[1];
            z = event.values[2];

            currentTime = new Date().getTime();

            if(currentTime-lastPositionUpdate > 6000){
                calibrateLinAcc();
                speed = 0.000;
            }

            Intent appDisplayData = new Intent();
            appDisplayData.setAction("com.teammerge.safetrip.APP_DISPLAY_DATA");
            appDisplayData.putExtra("speed",speed);
            appDisplayData.putExtra("speedLimit", speedLimit);
            appDisplayData.putExtra("lat", lat);
            appDisplayData.putExtra("lng", lng);
            appDisplayData.putExtra("time", currentTime-startTime);
            appDisplayData.putExtra("lengthOfFile", lengthOfFile);

            sendBroadcast(appDisplayData);
            String unCalVal = String.valueOf(Math.round(vectorLength(x, y, z)*100.0)/100.0);

            int screenVal = 0;
            if(ScreenStateReceiver.screenOn){
                screenVal = 1;
            }

            if(tripInProgress){
                appendLog((currentTime) + ", " +
                        speed + ", " +
                        speedLimit +", " +
                        lat   + ", " +
                        lng   + ", " +
                        x     + ", " +
                        y     + ", " +
                        z     + ", " +
                        unCalVal +", " +
                        screenVal);
                lengthOfFile++;
            }

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    PositioningManager.OnPositionChangedListener positionLister = new PositioningManager.OnPositionChangedListener() {
        @Override
        public void onPositionUpdated(PositioningManager.LocationMethod locationMethod, GeoPosition geoPosition, boolean b) {

            if(PositioningManager.getInstance().getRoadElement() == null && !fetchingDataInProgress){
                GeoBoundingBox areaAround = new GeoBoundingBox(geoPosition.getCoordinate(), 500, 500);
                fetchingDataInProgress = true;
                MapDataPrefetcher.getInstance().fetchMapData(areaAround);
            }

            if (geoPosition.isValid() && geoPosition instanceof MatchedGeoPosition) {



                MatchedGeoPosition mgp = (MatchedGeoPosition) geoPosition;
                lat = geoPosition.getCoordinate().getLatitude();
                lng = geoPosition.getCoordinate().getLongitude();
                double currentSpeed = Math.round((mgp.getSpeed() * 3.6) *100.0)/100.0;
                speed = currentSpeed;
                lastPositionUpdate = new Date().getTime();
                if(speed == 0){
                    calibrateLinAcc();
                }
                double currentSpeedLimit = 0;
                if (mgp.getRoadElement() != null) {
                    currentSpeedLimit = Math.round((mgp.getRoadElement().getSpeedLimit() * 3.6)*100.0)/100.0;
                    speedLimit = currentSpeedLimit;
                } else {
                    speedLimit = -1;
                }

                if(!tripInProgress){
                    Log.d("TRIP IN PROGRESS", "TRIP IN PROGRESS");
                    tripInProgress = true;
                }

            } else {
                //handle error
            }
        }

        @Override
        public void onPositionFixChanged(PositioningManager.LocationMethod locationMethod,
                                         PositioningManager.LocationStatus locationStatus) {

        }
    };

    MapDataPrefetcher.Adapter prefetcherListener = new MapDataPrefetcher.Adapter() {
        @Override
        public void onStatus(int requestId, PrefetchStatus status) {
            fetchingDataInProgress = false;

        }
    };

    public void appendLog(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("logData.txt", Context.MODE_APPEND));
            outputStreamWriter.append(data + "\n");
            outputStreamWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public double vectorLength (double x, double y, double z){
        return Math.sqrt(Math.pow(x, 2)+ Math.pow(y, 2)+  Math.pow(z, 2));
    }


    private void calibrateLinAcc(){
        xN = x;
        yN = y;
        zN = z;
    }

    @Override
    public void onDestroy(){
        Log.d("DESTROY", "Service is Destroyed");
        tripInProgress = false;
        sensorManager.unregisterListener(linAccSensorEvent);
        sensorManager.unregisterListener(gyroSensorEvent);
        sensorManager.unregisterListener(rotationSensorEvent);
        unregisterReceiver(mScreenStateReceiver);
        PositioningManager.getInstance().removeListener(positionLister);
        MapDataPrefetcher.getInstance().removeListener(prefetcherListener);
        PositioningManager.getInstance().stop();
        NavigationManager.getInstance().stop();
        super.onDestroy();

    }
}
