package com.teammerge.safetrip;

public class ContextViolation extends DriverError {
    private double duration;
    private double recommendedSpeedLimit;
    private LocationCoordinates startPoint;
    private LocationCoordinates endPoint;
    private Weather.WeatherConditions weather;


    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public double getRecommendedSpeedLimit() {
        return recommendedSpeedLimit;
    }

    public void setRecommendedSpeedLimit(double recommendedSpeedLimit) {
        this.recommendedSpeedLimit = recommendedSpeedLimit;
    }

    public LocationCoordinates getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(LocationCoordinates startPoint) {
        this.startPoint = startPoint;
    }

    public LocationCoordinates getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(LocationCoordinates endPoint) {
        this.endPoint = endPoint;
    }

    public Weather.WeatherConditions getWeather() {
        return weather;
    }

    public void setWeather(Weather.WeatherConditions weather) {
        this.weather = weather;
    }
}
