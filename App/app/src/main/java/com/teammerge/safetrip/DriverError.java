package com.teammerge.safetrip;

import com.google.firebase.firestore.Exclude;
import com.here.android.mpa.common.GeoCoordinate;

import java.util.Date;

public abstract class DriverError {
    private Date time;
    private double maxSpeed;
    private double averageSpeed;
    private double scoreImpact;

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public double getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(double averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public double getScoreImpact() {
        return scoreImpact;
    }

    public void setScoreImpact(double scoreImpact) {
        this.scoreImpact = scoreImpact;
    }
}