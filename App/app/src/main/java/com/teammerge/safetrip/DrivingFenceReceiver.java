package com.teammerge.safetrip;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.awareness.fence.FenceState;
import com.here.android.mpa.common.ApplicationContext;
import com.here.android.mpa.common.MapEngine;
import com.here.android.mpa.common.OnEngineInitListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class DrivingFenceReceiver extends BroadcastReceiver {

    private String headerText = "time, speed, speed.limit, lat, lng x.acc, y.acc, z.acc, total.acc, screen.on  \n";
    public Uri file;
    @Override
    public void onReceive(final Context context, final Intent intent) {
        Log.d("FENCELOG", "RECEIVED");
        FenceState fenceState = FenceState.extract(intent);
        if (TextUtils.equals(fenceState.getFenceKey(), "DRIVING_FENCE")) {
            switch (fenceState.getCurrentState()) {
                case FenceState.TRUE:
                    Log.d("FENCELOG", "FENCE STATE TRUE");
                    //initHEREEngine(context);
                    startTrip(true, context);
                    break;
                case FenceState.FALSE:
                    Log.d("FENCELOG", "FENCE STATE FALSE");
                    break;
                case FenceState.UNKNOWN:
                    Log.d("FENCELOG", "FENCE STATE UNKNOWN");
                    break;
            }
        }
    }
    public void startTrip(boolean auto, Context context) {
        if(MainActivity.getInstance() != null){
            if(MainActivity.getInstance().tripStopped == false){
                return;
            }
        }
        try {
            String rootDir = context.getFilesDir().getAbsolutePath();
            File uploadFile = new File(rootDir + "/logData.txt");
            file = Uri.fromFile(uploadFile);
            PrintWriter pw = new PrintWriter(file.getPath());
            if (auto) {
                pw.write("auto\n");
            }
            pw.write(headerText);
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if(MainActivity.getInstance() != null){
            MainActivity.getInstance().tripStarted = false;
            MainActivity.getInstance().tripStopped = false;
        }

        context.startForegroundService(new Intent(context, CarTrackingService.class));
    }

    public void initHEREEngine(final Context context){
        boolean success = com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(
                context.getExternalFilesDir(null) + File.separator + ".here-maps", "com.teammerge.safetrip");
        if(!success){
            Log.d("DISK FAIL", "Unable to set dish cache path");
        }
        else {
            MapEngine mapEngine = MapEngine.getInstance();
            ApplicationContext appContext = new ApplicationContext(context);
            mapEngine.init(appContext, new OnEngineInitListener() {
                @Override
                public void onEngineInitializationCompleted(Error error) {
                    if (error == OnEngineInitListener.Error.NONE) {
                        MapEngine.getInstance().onResume();
                        Log.d("ENGINE INIT", "ENGINE HAS STARTED");
                    } else {
                        Log.d("ENGINE INIT", error.getDetails());

                    }
                }
            });
        }
    }

}
