package com.teammerge.safetrip;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ExpandableListView;
import com.teammerge.safetrip.uiFragment.menu.ExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FaqActivity extends Activity {
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;


    List<String> listQuestion;
    HashMap<String, String> mapQuestionAnswer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        expListView = findViewById(R.id.expandableListView);

        listQuestion = new ArrayList<String>();


        listQuestion.add(getString(R.string.text_Question1));
        listQuestion.add(getString(R.string.text_Question2));
        listQuestion.add(getString(R.string.text_Question3));
        listQuestion.add(getString(R.string.text_Question4));
        listQuestion.add(getString(R.string.text_Question5));
        listQuestion.add(getString(R.string.text_Question6));
        listQuestion.add(getString(R.string.text_Question7));
        listQuestion.add(getString(R.string.text_Question8));
        listQuestion.add(getString(R.string.text_Question9));
        listQuestion.add(getString(R.string.text_Question10));
        listQuestion.add(getString(R.string.text_Question11));
        listQuestion.add(getString(R.string.text_Question12));

        mapQuestionAnswer = new HashMap<String, String>();

        mapQuestionAnswer.put(listQuestion.get(0), getString(R.string.text_Answer1));
        mapQuestionAnswer.put(listQuestion.get(1), getString(R.string.text_Answer2));
        mapQuestionAnswer.put(listQuestion.get(2), getString(R.string.text_Answer3));
        mapQuestionAnswer.put(listQuestion.get(3), getString(R.string.text_Answer4));
        mapQuestionAnswer.put(listQuestion.get(4), getString(R.string.text_Answer5));
        mapQuestionAnswer.put(listQuestion.get(5), getString(R.string.text_Answer6));
        mapQuestionAnswer.put(listQuestion.get(6), getString(R.string.text_Answer7));
        mapQuestionAnswer.put(listQuestion.get(7), getString(R.string.text_Answer8));
        mapQuestionAnswer.put(listQuestion.get(8), getString(R.string.text_Answer9));
        mapQuestionAnswer.put(listQuestion.get(9), getString(R.string.text_Answer10));
        mapQuestionAnswer.put(listQuestion.get(10), getString(R.string.text_Answer11));
        mapQuestionAnswer.put(listQuestion.get(11), getString(R.string.text_Answer12));

        listAdapter = new ExpandableListAdapter(this, mapQuestionAnswer, listQuestion);

        expListView.setAdapter(listAdapter);

    }


}
