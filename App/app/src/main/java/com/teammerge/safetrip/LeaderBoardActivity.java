package com.teammerge.safetrip;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.teammerge.safetrip.uiFragment.menu.LeaderBoardAdapter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LeaderBoardActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboard);

        // Get documents from firebase:
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("users")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<DocumentSnapshot> myListOfDocuments = task.getResult().getDocuments();
                            ArrayList<User> userList = new ArrayList<>();

                            if (!myListOfDocuments.isEmpty()){

                                User user;

                                for (DocumentSnapshot document : myListOfDocuments) {
                                    user = document.toObject(User.class);

                                    if(user.getEmail().equals("test@test.dk") || user.getEmail().equals("test@test.com") || user.getEmail().equals("super-admin@mail.com")) {
                                        continue;
                                    }
                                    userList.add(document.toObject(User.class));
                                }

                            }


                            Collections.sort(userList, new Comparator<User>() {
                                public int compare(User user1, User user2) {
                                    if (user1.getTotalScore() == user2.getTotalScore()) {
                                        return user1.getDisplayName().compareTo(user2.getDisplayName());
                                    }
                                    return (int) user2.getTotalScore() - (int) user1.getTotalScore();

                                }
                            });




                            ListView listView = findViewById(R.id.leaderBoardList);

                            LeaderBoardAdapter adapter = new LeaderBoardAdapter(getApplicationContext(), userList);

                            ListView itemsListView = (ListView) findViewById(R.id.leaderBoardList);
                            itemsListView.setAdapter(adapter);


                        }
                        else {
                            Log.d("test", "No users found");
                        }
                    }
                });


    }

}



