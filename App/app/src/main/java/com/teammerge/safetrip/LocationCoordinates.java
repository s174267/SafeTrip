package com.teammerge.safetrip;

import com.here.android.mpa.common.GeoCoordinate;

public class LocationCoordinates {

    private double lat;
    private double lng;

    public LocationCoordinates(){

    }

    public LocationCoordinates(double lat, double lng){
        this.lat = lat;
        this.lng = lng;

    }

    public GeoCoordinate toGeoCoord(){
        return new GeoCoordinate(this.lat, this.lng);
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
