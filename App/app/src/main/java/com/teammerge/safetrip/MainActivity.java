package com.teammerge.safetrip;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.awareness.Awareness;
import com.google.android.gms.awareness.fence.AwarenessFence;
import com.google.android.gms.awareness.fence.DetectedActivityFence;
import com.google.android.gms.awareness.fence.FenceState;
import com.google.android.gms.awareness.fence.FenceUpdateRequest;
import com.google.android.gms.awareness.fence.HeadphoneFence;
import com.google.android.gms.awareness.state.HeadphoneState;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.here.android.mpa.common.ApplicationContext;
import com.here.android.mpa.common.MapEngine;
import com.here.android.mpa.common.OnEngineInitListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;


public class MainActivity extends AppCompatActivity {

    private static MainActivity ins;
    public Uri file;

    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[] {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private final String API_KEY = "d6fa69abedb2248bf90b541b9ce712f1";
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    public boolean tripStopped = true;

    FirebaseStorage storage;

    private int lengthOfFile = 0;
    public boolean tripStarted = false;

    private String headerText = "time, speed, speed.limit, lat, lng x.acc, y.acc, z.acc, total.acc, screen.on  \n";

    BroadcastReceiver br;
    BroadcastReceiver waitForInternet;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        checkPermissions();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.teammerge.safetrip.APP_DISPLAY_DATA");
        registerReceiver(br, filter);
        ins = this;

    }

    public static MainActivity getInstance(){
        return ins;
    }

    public Map<String, Object> getFileWriteOptions(){
        Map<String, Object> fileOptions = new HashMap<>();
        String rootDir = this.getFilesDir().getAbsolutePath();
        File filenameViz = new File(rootDir + "/dataviz.txt");
        filenameViz.delete();

        File uploadFile = new File(rootDir + "/logData.txt");
        Uri file = Uri.fromFile(uploadFile);


        fileOptions.put("VIZFILE", Uri.fromFile(filenameViz));
        fileOptions.put("CACHEDIR", getCacheDir());
        try {
            fileOptions.put("FILE_OUTPUT_STREAM", openFileOutput("dataviz.txt", MODE_APPEND));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        fileOptions.put("CONTEXT", getApplicationContext());
        fileOptions.put("DATA_FILE", file);
        return fileOptions;
    }

    public void resetMainPage(){
        ProgressBar progressBar = findViewById(R.id.progressBarEndTrip);
        Button endTrip = findViewById(R.id.btn_endTrip);
        Button startTrip = findViewById(R.id.btn_startTrip);
        if(endTrip != null && startTrip != null && progressBar != null){
            endTrip.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.INVISIBLE);
            startTrip.setVisibility(View.VISIBLE);
            endTrip.setEnabled(true);
        }
    }

    public void updateDisplayText(final String speed, final String speedLimit, final double lat, final double lng, final String time, int length) {
        this.lengthOfFile = length;
        Button startTrip = findViewById(R.id.btn_startTrip);
        if(tripStopped == false || this.lengthOfFile >= 1){
            startTrip.setVisibility(View.INVISIBLE);
        }

        if(length > 100){
            Button endTrip = findViewById(R.id.btn_endTrip);
            endTrip.setText(R.string.end_trip);
            endTrip.setVisibility(View.VISIBLE);
            tripStarted = true;
        }
    }

    private void initialize(){
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        db.setFirestoreSettings(settings);

        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        boolean auto = sharedPref.getBoolean(getString(R.string.preference_file_key), true);

        if (auto) {
            registerFence();
        }
        else {
            Awareness.getFenceClient(this).updateFences(new FenceUpdateRequest.Builder()
                    .removeFence("DRIVING_FENCE")
                    .build());
        }



        db.collection("users").document(currentUser.getEmail())
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                // All is good
                                Log.d("test_TAG", "Document exists");
                            } else {
                                // Create the document
                                Log.d("test_TAG", "No such document. Creating the document");

                                User internalUser = new User(currentUser.getDisplayName(), currentUser.getEmail());
                                FirebaseFirestore db = FirebaseFirestore.getInstance();

                                db.collection("users").document(currentUser.getEmail())
                                        .set(internalUser, SetOptions.merge())
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Log.d("test_TAG", "createUserUploadInternalUser:success");
                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Log.d("tes_TAG", "createUserUploadInternalUser:failure");
                                            }
                                        });
                            }
                        } else {
                            Log.d("test_TAG", "get failed with ", task.getException());
                        }
                    }
                });

        setContentView(R.layout.activity_main);
        initHEREEngine();
        BottomNavigationView navView = findViewById(R.id.nav_view);

        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications).build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(navView, navController);

        NotificationChannel channel = new NotificationChannel("42", "SafeTrip Driving Assessment", NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription("SafeTrip is monitoring your driving");

        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(channel);

        br = new AppDisplayUpdate();

        storage = FirebaseStorage.getInstance();

        String rootDir = this.getFilesDir().getAbsolutePath();
        File uploadFile = new File(rootDir + "/logData.txt");
        file = Uri.fromFile(uploadFile);



    }

    public void registerFence(){
       // AwarenessFence drivingFence = HeadphoneFence.during(HeadphoneState.PLUGGED_IN);
        AwarenessFence drivingFence = DetectedActivityFence.during(DetectedActivityFence.IN_VEHICLE);
        PendingIntent fenceIntent = PendingIntent.getBroadcast(this,
                0,
                new Intent(this, DrivingFenceReceiver.class),
                0);

        Awareness.getFenceClient(this).updateFences(new FenceUpdateRequest.Builder()
                .addFence("DRIVING_FENCE", drivingFence, fenceIntent)
                .build())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.i("FENCELOG", "Fence was successfully registered.");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("FENCELOG", "Fence could not be registered: " + e);
                    }
                });
    }

    public void initHEREEngine(){
        boolean success = com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(
                getApplicationContext().getExternalFilesDir(null) + File.separator + ".here-maps", "com.teammerge.safetrip");
        if(!success){
            Toast.makeText(getApplicationContext(), "Unable to set isolated disk cache path.", Toast.LENGTH_LONG);
            Log.d("DISK FAIL", "Unable to set dish cache path");
        }
        else {
            MapEngine mapEngine = MapEngine.getInstance();
            ApplicationContext appContext = new ApplicationContext(getApplicationContext());

            mapEngine.init(appContext, new OnEngineInitListener() {
                @Override
                public void onEngineInitializationCompleted(Error error) {
                    if (error == OnEngineInitListener.Error.NONE) {
                        MapEngine.getInstance().onResume();
                        Log.d("ENGINE INIT", "ENGINE HAS STARTED");
                    } else {
                        Log.d("ENGINE INIT", error.getDetails());

                    }
                }
            });
        }
    }


    public void startTrip(boolean auto) {

        if(tripStopped == false){
            return;
        }
        try {
            PrintWriter pw = new PrintWriter(file.getPath());
            if (auto){
                pw.write("auto\n");
            } else {
                pw.write("not-auto\n");
                Button endTrip = findViewById(R.id.btn_endTrip);
                endTrip.setText(R.string.abort_trip);
                endTrip.setVisibility(View.VISIBLE);
            }
            pw.write(headerText);
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        tripStarted = false;
        tripStopped = false;
        startService(new Intent(getApplicationContext(), CarTrackingService.class));
    }


    @SuppressLint("RestrictedApi")
    public void endTrip() {
        tripStopped = true;
        if (tripStarted == false) {
            stopService(new Intent(getApplicationContext(), CarTrackingService.class));
            ProgressBar progressBar = findViewById(R.id.progressBarEndTrip);
            Button endTrip = findViewById(R.id.btn_endTrip);
            Button startTrip = findViewById(R.id.btn_startTrip);

            endTrip.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.INVISIBLE);
            startTrip.setVisibility(View.VISIBLE);
            endTrip.setEnabled(true);
            registerFence();
            return;
        }
        StorageReference storageRef = storage.getReference();
        String filename = currentUser.getEmail() + "/logData/" + new Date().toString() +".txt";
        StorageReference logData = storageRef.child(filename);
        UploadTask uploadTask = logData.putFile(file);

        stopService(new Intent(getApplicationContext(), CarTrackingService.class));


        Data data = new Data.Builder()
                .putInt("LENGTH_OF_FILE", lengthOfFile)
                .build();

        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        OneTimeWorkRequest tripWorker = new OneTimeWorkRequest.Builder(TripEvaluation.class)
                .setConstraints(constraints)
                .setInputData(data)
                .build();

        WorkManager.getInstance(getApplicationContext()).enqueue(tripWorker);
        registerFence();

    }

    protected void checkPermissions(){
        final List<String> missingPermissions = new ArrayList<String>();
        for(final String permission : REQUIRED_SDK_PERMISSIONS){
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if(result != PackageManager.PERMISSION_GRANTED){
                missingPermissions.add(permission);
            }
        }
        if(!missingPermissions.isEmpty()){
            final String[] permissions = missingPermissions.toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        }
        else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS, grantResults);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        // exit the app if one permission is not granted
                        Toast.makeText(MainActivity.this, getString(R.string.text_RequiredPermission) + permissions[index]
                                + getString(R.string.text_NotGrantedExiting), Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                }
                initialize();
                break;
        }
    }
    @Override
    public void onDestroy(){
        unregisterReceiver(br);
        super.onDestroy();
    }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}

