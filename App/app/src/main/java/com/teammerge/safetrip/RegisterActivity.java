package com.teammerge.safetrip;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

public class RegisterActivity extends Activity {

    private FirebaseAuth mAuth;
    private EditText emailField;
    private EditText passwordField;
    private EditText displayField;
    private Button btnCreateUser;
    private ProgressBar progressBar;

    private static final String TAG_LOGIN = "CurrentUser_Login";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        emailField = findViewById(R.id.registerEmailField);
        passwordField = findViewById(R.id.registerPaswordField);
        btnCreateUser = findViewById(R.id.btn_signUp);
        displayField = findViewById(R.id.registerDisplayName);
        progressBar = findViewById(R.id.progressBar);
        mAuth = FirebaseAuth.getInstance();

        btnCreateUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAccount(emailField.getText().toString(), passwordField.getText().toString(),displayField.getText().toString());
            }
        });


    }

    private void createAccount(final String email, final String password, final String displayName) {
        if (!isValidEmail(email)) {
            Toast.makeText(RegisterActivity.this, R.string.email_is_not_valid,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if (!isValidPassword(password)) {
            Toast.makeText(RegisterActivity.this, R.string.password_is_not_valid,
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if (!isValidDisplayName(displayName)) {
            Toast.makeText(RegisterActivity.this, R.string.display_name_is_not_valid,
                    Toast.LENGTH_SHORT).show();
            return;
        }

        progressBar.setVisibility(View.VISIBLE);
        btnCreateUser.setEnabled(false);

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG_LOGIN, "createUserWithEmail:success");

                            User internalUser = new User(displayName, email);
                            FirebaseFirestore db = FirebaseFirestore.getInstance();


                            db.collection("users").document(email)
                                    .set(internalUser, SetOptions.merge())
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Log.d(TAG_LOGIN, "createUserUploadInternalUser:success");
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.d(TAG_LOGIN, "createUserUploadInternalUser:failure");
                                        }
                                    });



                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder().setDisplayName(displayName).build();
                            user.updateProfile(profileUpdates);


                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } else {
                            Log.d(TAG_LOGIN, "createUserWithEmail:failure");
                            Toast.makeText(RegisterActivity.this, R.string.toast_AuthenticationFailed,
                                    Toast.LENGTH_SHORT).show();
                        }
                        progressBar.setVisibility(View.INVISIBLE);
                        btnCreateUser.setEnabled(true);
                    }
                });
    }

    private boolean isValidPassword(String password) {
        return (password.length() > 5);
    }

    private boolean isValidEmail(String email) {
        String emailRegex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(emailRegex);
    }

    private boolean isValidDisplayName(String displayname) {
        if (displayname.length() >= 2 && displayname.length() <= 25) {
            return true;
        }
        return false;

    }

}
