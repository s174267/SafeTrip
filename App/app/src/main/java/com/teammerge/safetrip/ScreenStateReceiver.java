package com.teammerge.safetrip;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ScreenStateReceiver extends BroadcastReceiver {

    public static boolean screenOn = false;
    @Override
    public void onReceive(Context context, Intent intent) {

        if(intent.getAction().equals(Intent.ACTION_USER_PRESENT)){
            screenOn = true;
        }
        else if(intent.getAction().equals(Intent.ACTION_SCREEN_OFF)){
            screenOn = false;
        }

    }
}
