package com.teammerge.safetrip;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.awareness.Awareness;
import com.google.android.gms.awareness.fence.FenceUpdateRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.teammerge.safetrip.R;

public class SettingsActivity extends AppCompatActivity {

    Context context;
    SharedPreferences sharedPref;
    FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    EditText displayName;
    TextView displayNameValid;
    User internalUser;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        db = FirebaseFirestore.getInstance();


        context = getApplicationContext();
        sharedPref  = context.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        final Switch autoStartSwitch = findViewById(R.id.switch_autostart);
        boolean auto = sharedPref.getBoolean(getString(R.string.preference_file_key), true);
        displayName = findViewById(R.id.ChangeDisplayName);
        displayNameValid = findViewById(R.id.displayNameValid);




        db.collection("users").document(mAuth.getCurrentUser().getEmail())
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        internalUser = document.toObject(User.class);
                        displayName.setText(internalUser.getDisplayName());
                    } else {
                        displayName.setText("");
                    }
                } else {
                    Log.d("test_TAG", "get failed with ", task.getException());
                    displayName.setText("");
                }
            }
        });



        displayName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(isValidDisplayName(String.valueOf(s))){
                    displayNameValid.setVisibility(View.INVISIBLE);
                    internalUser.setDisplayName(String.valueOf(s));

                    db.collection("users").document(mAuth.getCurrentUser().getEmail())
                            .set(internalUser, SetOptions.merge())
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.d("test", "updateDisplayName:success");
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("test", "updateDisplayName:failure");
                        }
                    });


                } else {
                    displayNameValid.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });





        autoStartSwitch.setChecked(auto);

        autoStartSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(getString(R.string.preference_file_key), isChecked);
                editor.apply();


                if(isChecked){
                    if(MainActivity.getInstance() != null){
                        MainActivity.getInstance().registerFence();
                    }
                }
                else {
                    Awareness.getFenceClient(getApplicationContext()).updateFences(new FenceUpdateRequest.Builder()
                            .removeFence("DRIVING_FENCE")
                            .build());
                }
            }
        });


    }

    private boolean isValidDisplayName(String displayname) {
        if (displayname.length() >= 2 && displayname.length() <= 25) {
            return true;
        }
        return false;

    }
















}
