package com.teammerge.safetrip;

public class SpeedError extends DriverError{
    private double duration;
    private double speedLimit;
    private LocationCoordinates startPoint;
    private LocationCoordinates endPoint;

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public double getSpeedLimit() {
        return speedLimit;
    }

    public void setSpeedLimit(double speedLimit) {
        this.speedLimit = speedLimit;
    }

    public LocationCoordinates getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(LocationCoordinates startPoint) {
        this.startPoint = startPoint;
    }

    public LocationCoordinates getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(LocationCoordinates endPoint) {
        this.endPoint = endPoint;
    }
}
