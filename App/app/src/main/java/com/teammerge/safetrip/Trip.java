package com.teammerge.safetrip;

import java.util.ArrayList;
import java.util.Date;

public class Trip implements Comparable<Trip> {
    private Date startTime;
    private Date endTime;
    private double length;
    private ArrayList<LocationCoordinates> route;
    private ArrayList<SpeedError> speedViolations;
    private ArrayList<ContextViolation> contextViolations;
    private ArrayList<Weather> weather;
    private double score;
    private double jerkScore;
    private double speedScore;
    private double contextScore;
    private double distractionScore;
    private String startCity;
    private String endCity;
    private String id;
    private boolean serialized = false;
    private boolean isApproved = false;
    private double averageSpeed;
    private double relativeSpeedAverage;
    private double percentageScreenOn;
    private int numberOfScreenOns;



    public Trip(){
        route = new ArrayList<>();
        speedViolations = new ArrayList<>();
        contextViolations = new ArrayList<>();
        weather = new ArrayList<>();
    }

    public double getCalcDuration(){
        return (this.getEndTime().getTime() - this.getStartTime().getTime()) / 1000.0;
    }

    public void generateId(){
        this.id = String.valueOf(this.startTime.getTime());
    }

    @Override
    public int compareTo(Trip trip){
        return Long.compare(this.startTime.getTime(), trip.getStartTime().getTime());
    }



    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public ArrayList<LocationCoordinates> getRoute() {
        return route;
    }

    public void setRoute(ArrayList<LocationCoordinates> route) {
        this.route = route;
    }

    public ArrayList<SpeedError> getSpeedViolations() {
        return speedViolations;
    }

    public void setSpeedViolations(ArrayList<SpeedError> speedViolations) {
        this.speedViolations = speedViolations;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getJerkScore() {
        return jerkScore;
    }

    public void setJerkScore(double jerkScore) {
        this.jerkScore = jerkScore;
    }

    public String getEndCity() {
        return endCity;
    }

    public void setEndCity(String endCity) {
        this.endCity = endCity;
    }

    public String getStartCity() {
        return startCity;
    }

    public void setStartCity(String startCity) {
        this.startCity = startCity;
    }

    public ArrayList<Weather> getWeather() {
        return weather;
    }

    public void setWeather(ArrayList<Weather> weather) {
        this.weather = weather;
    }

    public double getSpeedScore() {
        return speedScore;
    }

    public void setSpeedScore(double speedScore) {
        this.speedScore = speedScore;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSerialized() {
        return serialized;
    }

    public void setSerialized(boolean serialized) {
        this.serialized = serialized;
    }

    public ArrayList<ContextViolation> getContextViolations() {
        return contextViolations;
    }

    public void setContextViolations(ArrayList<ContextViolation> contextViolations) {
        this.contextViolations = contextViolations;
    }

    public double getContextScore() {
        return contextScore;
    }

    public void setContextScore(double contextScore) {
        this.contextScore = contextScore;
    }

    public double getDistractionScore() {
        return distractionScore;
    }

    public void setDistractionScore(double distractionScore) {
        this.distractionScore = distractionScore;
    }

    public double getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(double averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public double getRelativeSpeedAverage() {
        return relativeSpeedAverage;
    }

    public void setRelativeSpeedAverage(double relativeSpeedAverage) {
        this.relativeSpeedAverage = relativeSpeedAverage;
    }

    public int getNumberOfScreenOns() {
        return numberOfScreenOns;
    }

    public void setNumberOfScreenOns(int numberOfScreenOns) {
        this.numberOfScreenOns = numberOfScreenOns;
    }

    public double getPercentageScreenOn() {
        return percentageScreenOn;
    }

    public void setPercentageScreenOn(double percentageScreenOn) {
        this.percentageScreenOn = percentageScreenOn;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public void setApproved(boolean approved) {
        isApproved = approved;
    }
}
