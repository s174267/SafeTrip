package com.teammerge.safetrip;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.strictmode.Violation;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.ListResult;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.ViewObject;
import com.here.android.mpa.mapping.AndroidXMapFragment;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapGesture;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapObject;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.CoreRouter;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RouteResult;
import com.here.android.mpa.routing.RoutingError;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

public class TripDetailsActivity extends FragmentActivity {
    private Map map = null;
    Trip trip;
    ArrayList<MapRoute> violations = new ArrayList<>();

    HashMap<MapRoute, SpeedError> violationMarkerMap = new HashMap<>();

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseStorage storage = FirebaseStorage.getInstance();
    FirebaseAuth mAuth = FirebaseAuth.getInstance();

    // map fragment embedded in this activity
    private AndroidXMapFragment mapFragment = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Gson gson = new Gson();
        trip = gson.fromJson(getIntent().getStringExtra("trip"), Trip.class);

        initialize();
    }

    public void createRoute(){
        ArrayList<GeoCoordinate> points = new ArrayList<>();
        for(LocationCoordinates coord: trip.getRoute()){
            points.add(coord.toGeoCoord());
        }

        RouteOptions routeOptions = new RouteOptions();
        routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
        routeOptions.setRouteType(RouteOptions.Type.SHORTEST);

        CoreRouter coreRouter = new CoreRouter();
        coreRouter.calculateRoute(points, routeOptions, new CoreRouter.Listener() {
            @Override
            public void onCalculateRouteFinished(List<RouteResult> list, RoutingError routingError) {
                if (routingError == RoutingError.NONE) {
                    RouteResult result = list.get(0);
                    Route resultRoute = result.getRoute();
                    MapRoute mapRoute = new MapRoute(resultRoute);
                    mapRoute.setColor(Color.GREEN);
                    GeoBoundingBox gbb = resultRoute.getBoundingBox();
                    map.addMapObject(mapRoute);
                    gbb.expand(2000,2000);
                    map.zoomTo(gbb, Map.Animation.NONE, 0);

                    Route.serializeAsync(resultRoute, new Route.SerializationCallback() {
                        @Override
                        public void onSerializationComplete(Route.SerializationResult serializationResult) {
                            byte[] mapSerialized = serializationResult.data;
                            StorageReference storageRef = storage.getReference();
                            String filename = mAuth.getCurrentUser().getEmail() + "/routes/"+ trip.getId() + "/" + "route.route";
                            StorageReference logData = storageRef.child(filename);
                            logData.putBytes(mapSerialized);
                        }
                    });

                    markSpeedErrors();
                }
            }
            @Override
            public void onProgress(int i) {
            }
        });
    }

    public void markSpeedErrors(){
        RouteOptions routeOptions = new RouteOptions();
        routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
        routeOptions.setRouteType(RouteOptions.Type.SHORTEST);

        for(final SpeedError se : trip.getSpeedViolations()){
            ArrayList<GeoCoordinate> points = new ArrayList<>();
            points.add(se.getStartPoint().toGeoCoord());
            points.add(se.getEndPoint().toGeoCoord());

            CoreRouter coreRouter = new CoreRouter();
            coreRouter.calculateRoute(points, routeOptions, new CoreRouter.Listener() {
                @Override
                public void onCalculateRouteFinished(List<RouteResult> list, RoutingError routingError) {
                    if (routingError == RoutingError.NONE) {
                        RouteResult result = list.get(0);
                        Route resultRoute = result.getRoute();
                        MapRoute mapRoute = new MapRoute(resultRoute);
                        double violationVal = se.getAverageSpeed()-se.getSpeedLimit();
                        mapRoute.setColor(getRouteViolationColor(violationVal));
                        violationMarkerMap.put(mapRoute, se);
                        map.addMapObject(mapRoute);

                        Route.serializeAsync(resultRoute, new Route.SerializationCallback() {
                            @Override
                            public void onSerializationComplete(Route.SerializationResult serializationResult) {
                                byte[] mapSerialized = serializationResult.data;
                                StorageReference storageRef = storage.getReference();
                                String filename = mAuth.getCurrentUser().getEmail() + "/routes/"+trip.getId() +"/" + trip.getSpeedViolations().indexOf(se) +".route";
                                StorageReference logData = storageRef.child(filename);
                                logData.putBytes(mapSerialized);
                            }
                        });

                    }
                }
                @Override
                public void onProgress(int i) {
                }
            });


        }
        trip.setSerialized(true);
        db.collection(mAuth.getCurrentUser().getEmail()+ "-trips").document(trip.getId())
                .set(trip, SetOptions.merge())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("FAIL", "Error adding document", e);
                    }
                });

    }

    private int getRouteViolationColor(double value){
        int red = 255;
        int green;
        int blue = 0;

        double scoreFraction = value/15;
        if(scoreFraction > 1){
            scoreFraction  =1;
        }
        green = (int) (255*(1.0 - ((scoreFraction-0.5)*2.0)));

        return Color.rgb((red), (green), blue);

    }

    private void setScoreColor(LinearLayout tv, double score){
        int red;
        int green;
        int blue = 0;
        double scoreFraction = score/100;
        if(scoreFraction<0.5){
            red = 255;
        }
        else {
            red = (int) (255*(1.0 - ((scoreFraction-0.5)*2.0)));
        }
        if(scoreFraction <0.5){
            green = (int) (255*(scoreFraction*2));
        }
        else{
            green = 255;
        }
        Drawable background = AppCompatResources.getDrawable(getApplicationContext(), R.drawable.layout_border);
        if(score < 0){
            background.setTint(Color.rgb((int)(230*0.8), (int)(0*0.8), blue));
            tv.setBackground(background);

        }
        else {
            background.setTint(Color.rgb((int)(red*0.8), (int)(green*0.8), blue));
            tv.setBackground(background);

        }
    }


    private void loadRoute(){
        StorageReference listRef = storage.getReference().child(mAuth.getCurrentUser().getEmail()+"/routes/" + trip.getId());
        listRef.listAll()
                .addOnSuccessListener(new OnSuccessListener<ListResult>() {
                    @Override
                    public void onSuccess(ListResult listResult) {
                        for(StorageReference item : listResult.getItems()){
                            if(item.getName().equals("route.route")){
                                item.getBytes(1024*1024*10).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                                    @Override
                                    public void onSuccess(byte[] bytes) {
                                        Route.deserializeAsync(bytes, new Route.DeserializationCallback() {
                                            @Override
                                            public void onDeserializationComplete(Route.DeserializationResult deserializationResult) {
                                                if(deserializationResult.route != null){
                                                    MapRoute mapRoute = new MapRoute(deserializationResult.route);
                                                    mapRoute.setColor(Color.GREEN);
                                                    GeoBoundingBox gbb = deserializationResult.route.getBoundingBox();
                                                    map.addMapObject(mapRoute);
                                                    gbb.expand(2000,2000);
                                                    map.zoomTo(gbb, Map.Animation.NONE, 0);
                                                    loadViolations();
                                                }
                                                else {
                                                    createRoute();
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        }

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
    }

    private void loadViolations(){
        StorageReference listRef = storage.getReference().child(mAuth.getCurrentUser().getEmail()+"/routes/" + trip.getId());
        listRef.listAll()
                .addOnSuccessListener(new OnSuccessListener<ListResult>() {
                    @Override
                    public void onSuccess(ListResult listResult) {
                        for(final StorageReference item : listResult.getItems()){
                            if(!item.getName().equals("route.route")){
                                item.getBytes(1024*1024*10).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                                    @Override
                                    public void onSuccess(byte[] bytes) {
                                        Route.deserializeAsync(bytes, new Route.DeserializationCallback() {
                                            @Override
                                            public void onDeserializationComplete(Route.DeserializationResult deserializationResult) {
                                                if(deserializationResult.route != null){
                                                    MapRoute mapRoute = new MapRoute(deserializationResult.route);

                                                    String str = item.getName();
                                                    String name = str.substring(0, str.length()-6);
                                                    if(trip.getSpeedViolations().size() != 0){
                                                        SpeedError se = trip.getSpeedViolations().get(Integer.parseInt(name));
                                                        violationMarkerMap.put(mapRoute, se);
                                                        double violationVal = se.getMaxSpeed()-se.getSpeedLimit();
                                                        mapRoute.setColor(getRouteViolationColor(violationVal));
                                                    }
                                                    map.addMapObject(mapRoute);
                                                }
                                                else {
                                                    markSpeedErrors();
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        }

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
    }



    private void initialize() {
        setContentView(R.layout.activity_trip_details);

        // Search for the map fragment to finish setup by calling init().
        mapFragment = (AndroidXMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragment);


        TextView tripTitle = findViewById(R.id.lbl_tripDetailsTitle);
        TextView speedScore = findViewById(R.id.lbl_tripDetailsSpeedScore);
        TextView jerkScore = findViewById(R.id.lbl_tripDetailsAggScore);
        TextView weatherScore = findViewById(R.id.lbl_tripDetailsWeatherScore);
        TextView distractionScore = findViewById(R.id.lbl_tripDetailsPhoneScore);
        TextView totalScore = findViewById(R.id.lbl_tripDetailsTotalScore);


        LinearLayout speedScoreContainer = findViewById(R.id.layout_speedScore);
        LinearLayout jerkScoreContainer = findViewById(R.id.layout_jerkScore);
        LinearLayout weatherScoreContainer = findViewById(R.id.layout_weatherScore);
        LinearLayout distractionScoreContainer = findViewById(R.id.layout_phoneScore);
        LinearLayout totalScoreContainer = findViewById(R.id.layout_totalScore);
        LinearLayout approveButtonContainer = findViewById(R.id.btn_yes);
        LinearLayout declineButtonContainer = findViewById(R.id.btn_no);
        final LinearLayout approvedViewContainer = findViewById(R.id.wereYouDriving);
        final LinearLayout scoreDetailsViewContainer = findViewById(R.id.scoreDetailsView);


        if (trip.isApproved()){
            scoreDetailsViewContainer.setVisibility(View.VISIBLE);
            approvedViewContainer.setVisibility(View.GONE);
        } else {
            approvedViewContainer.setVisibility(View.VISIBLE);
            scoreDetailsViewContainer.setVisibility(View.GONE);
        }

        approveButtonContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trip.setApproved(true);
                db.collection(mAuth.getCurrentUser().getEmail()+ "-trips").document(trip.getId())
                        .set(trip, SetOptions.merge())
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                scoreDetailsViewContainer.setVisibility(View.VISIBLE);
                                approvedViewContainer.setVisibility(View.GONE);
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w("FAIL", "Error adding document", e);
                            }
                        });
            }
        });

        declineButtonContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trip.setApproved(false);
                db.collection(mAuth.getCurrentUser().getEmail()+ "-trips").document(trip.getId())
                        .delete()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                finish();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w("Test", "Error deleting document", e);
                            }
                        });
            }
        });



        speedScoreContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View mapView = mapFragment.getView();

                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                View popupView = inflater.inflate(R.layout.speeding_popup_window, null);

                // Creating the popup window
                final PopupWindow popupWindow = new PopupWindow(popupView, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);


                // Finding the worst speed violation.
                double worstViolationScore = -1;
                SpeedError worstViolationError = null;

                for (SpeedError violation: trip.getSpeedViolations()) {
                    if (violation.getScoreImpact() > worstViolationScore) {
                        worstViolationScore = violation.getScoreImpact();
                        worstViolationError = violation;
                    }
                }

                // Settings values for popup window
                TextView numberOfSpeedViolations = popupView.findViewById(R.id.lbl_number_of_speed_violations);
                TextView averageMovingSpeed = popupView.findViewById(R.id.lbl_average_moving_speed);
                TextView averageRelativeSpeed = popupView.findViewById(R.id.lbl_average_relative_speed);
                TextView worstViolation = popupView.findViewById(R.id.lbl_worst_speed_violation);

                numberOfSpeedViolations.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%s %d", getResources().getString(R.string.text_number_of_violations), trip.getSpeedViolations().size()));
                averageMovingSpeed.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()), "%s %.0f %s", getResources().getString(R.string.text_average_moving_speed), trip.getAverageSpeed(), getResources().getString(R.string.text_kmh)));
                averageRelativeSpeed.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%s %.0f%%", getResources().getString(R.string.text_average_relative_speed), trip.getRelativeSpeedAverage() * 100));

                if (worstViolationScore == -1) {
                    worstViolation.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%s %s", getResources().getString(R.string.text_worst_violation) , getString(R.string.text_no_violations)));
                } else {
                    worstViolation.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%s %.0f %s %s %.0f %s %s", getResources().getString(R.string.text_worst_violation), worstViolationError.getAverageSpeed(), getResources().getString(R.string.text_kmh), getString(R.string.text_in_a) ,worstViolationError.getSpeedLimit(), getResources().getString(R.string.text_kmh), getString(R.string.text_zone)));
                }

                // show the popup window
                popupWindow.showAtLocation(mapView, Gravity.CENTER, 0,-200);

            }
        });



        jerkScoreContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View mapView = mapFragment.getView();

                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                View popupView = inflater.inflate(R.layout.jerk_popup_window, null);

                // Creating the popup window
                final PopupWindow popupWindow = new PopupWindow(popupView, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);

                // show the popup window
                popupWindow.showAtLocation(mapView, Gravity.CENTER, 0,-200);

            }
        });


        distractionScoreContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View mapView = mapFragment.getView();

                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                View popupView = inflater.inflate(R.layout.distraction_popup_window, null);

                // Creating the popup window
                final PopupWindow popupWindow = new PopupWindow(popupView, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);


                // Settings values for popup window
                TextView numberOfScreenOns = popupView.findViewById(R.id.lbl_number_of_screen_ons);
                TextView screenOnTime = popupView.findViewById(R.id.lbl_screen_on_time);

                numberOfScreenOns.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%s %d", getResources().getString(R.string.text_number_of_screen_ons), trip.getNumberOfScreenOns()));
                screenOnTime.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()), "%s %.0f%%", getResources().getString(R.string.text_screen_on_time), trip.getPercentageScreenOn()));


                // show the popup window
                popupWindow.showAtLocation(mapView, Gravity.CENTER, 0,-200);

            }
        });


        totalScoreContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View mapView = mapFragment.getView();

                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                View popupView = inflater.inflate(R.layout.total_score_popup_window, null);

                // Creating the popup window
                final PopupWindow popupWindow = new PopupWindow(popupView, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);


                // Converting date format
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.forLanguageTag(Locale.getDefault().toLanguageTag()));
                String startTimeString = simpleDateFormat.format(trip.getStartTime());
                String endTimeString = simpleDateFormat.format(trip.getEndTime());


                // Converting trip duration
                String duration = "";
                if (trip.getCalcDuration() > 60*60){
                    int hour = (int) (trip.getCalcDuration()/(60*60));
                    int minutes = (int) ((trip.getCalcDuration()%(60*60))/60);

                    duration = hour + " " + getResources().getString(R.string.text_hour_symbol) + minutes + " min";
                }
                else {
                    duration = (int)(trip.getCalcDuration()/60) + " min";
                }


                // Settings values for popup window
                TextView tripDistance = popupView.findViewById(R.id.lbl_trip_distance);
                TextView tripDuration = popupView.findViewById(R.id.lbl_trip_duration);
                TextView tripStartTime = popupView.findViewById(R.id.lbl_trip_start_time);
                TextView tripEndTime = popupView.findViewById(R.id.lbl_trip_end_time);

                tripDistance.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%s %.2f %s", getResources().getString(R.string.text_trip_distance), trip.getLength(), getString(R.string.text_km)));
                tripDuration.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()), "%s %s", getResources().getString(R.string.text_trip_duration), duration));
                tripStartTime.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%s %s", getResources().getString(R.string.text_trip_start_time), startTimeString));
                tripEndTime.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%s %s", getResources().getString(R.string.text_trip_end_time), endTimeString));


                // show the popup window
                popupWindow.showAtLocation(mapView, Gravity.CENTER, 0,-200);

            }
        });


        weatherScoreContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View mapView = mapFragment.getView();

                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                View popupView = inflater.inflate(R.layout.weather_popup_window, null);

                // Creating the popup window
                final PopupWindow popupWindow = new PopupWindow(popupView, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);


                // Finding the worst weather violation.
                double worstViolationScore = -1;
                ContextViolation worstViolationError = null;

                for (ContextViolation violation: trip.getContextViolations()) {
                    if (violation.getScoreImpact() > worstViolationScore) {
                        worstViolationScore = violation.getScoreImpact();
                        worstViolationError = violation;
                    }
                }

                // Converting the weather data to single strings
                ArrayList<String> weatherStrings = new ArrayList<>();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.forLanguageTag(Locale.getDefault().toLanguageTag()) );

                for (Weather weather: trip.getWeather()) {
                    if (weather.getTimeStamp() == null) {
                        switch (weather.getWeatherCondition()){
                            case CLEAR_AND_DRY:
                                weatherStrings.add(getString(R.string.text_clear_and_dry));
                                break;
                            case LIGHT_RAIN_AND_LIGHT_SNOW:
                                weatherStrings.add(getString(R.string.text_light_rain_or_light_snow));
                                break;
                            case HEAVY_RAIN:
                                weatherStrings.add(getString(R.string.text_heavy_rain));
                                break;
                            case HEAVY_SNOW:
                                weatherStrings.add(getString(R.string.text_heavy_snow));
                                break;
                            case FREEZING:
                                weatherStrings.add(getString(R.string.text_freezing));
                                break;
                            default:
                                weatherStrings.add("None");
                        }

                    } else {
                        String date = simpleDateFormat.format(weather.getTimeStamp());
                        switch (weather.getWeatherCondition()){
                            case CLEAR_AND_DRY:
                                weatherStrings.add(getString(R.string.text_clear_and_dry) + " " + getString(R.string.text_at_time) + " " + date);
                                break;
                            case LIGHT_RAIN_AND_LIGHT_SNOW:
                                weatherStrings.add(getString(R.string.text_light_rain_or_light_snow) + " " + getString(R.string.text_at_time) + " " + date);
                                break;
                            case HEAVY_RAIN:
                                weatherStrings.add(getString(R.string.text_heavy_rain) + " " + getString(R.string.text_at_time) + " " + date);
                                break;
                            case HEAVY_SNOW:
                                weatherStrings.add(getString(R.string.text_heavy_snow) + " " + getString(R.string.text_at_time) + " " + date);
                                break;
                            case FREEZING:
                                weatherStrings.add(getString(R.string.text_freezing) + " " + getString(R.string.text_at_time) + " " + date);
                                break;
                            default:
                                weatherStrings.add("None");
                        }

                    }
                }


                // Settings values for popup window
                TextView numberOfWeatherViolations = popupView.findViewById(R.id.lbl_number_of_weather_violations);
                TextView worstWeatherViolation = popupView.findViewById(R.id.lbl_worst_weather_violation);

                numberOfWeatherViolations.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%s %d", getResources().getString(R.string.text_number_of_violations), trip.getContextViolations().size()));

                if (worstViolationScore == -1) {
                    worstWeatherViolation.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%s %s", getResources().getString(R.string.text_worst_violation), getString(R.string.text_no_violations)));
                } else {
                    worstWeatherViolation.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%s %.0f %s %s %s, %s: %.0f %s", getResources().getString(R.string.text_worst_weather_violation), worstViolationError.getAverageSpeed(), getResources().getString(R.string.text_kmh), getString(R.string.text_in_a), getString(R.string.text_zone), getString(R.string.text_with_recommended_speed), worstViolationError.getRecommendedSpeedLimit(), getResources().getString(R.string.text_kmh)));
                }


                ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.weather_list_item , weatherStrings);
                ListView listView = (ListView) popupView.findViewById(R.id.lbl_list_of_weather);
                listView.setAdapter(itemsAdapter);


                // show the popup window
                popupWindow.showAtLocation(mapView, Gravity.CENTER, 0,-200);
            }
        });



        tripTitle.setText(getResources().getString(R.string.text_trip_from) + trip.getStartCity() + getResources().getString(R.string.text_to) + trip.getEndCity());

        speedScore.setText(String.valueOf(Math.round(trip.getSpeedScore())));
        setScoreColor(speedScoreContainer, trip.getSpeedScore());

        jerkScore.setText(String.valueOf(Math.round(trip.getJerkScore())));
        setScoreColor(jerkScoreContainer, trip.getJerkScore());

        weatherScore.setText(String.valueOf((Math.round(trip.getContextScore()))));
        setScoreColor(weatherScoreContainer, trip.getContextScore());

        distractionScore.setText(String.valueOf(Math.round(trip.getDistractionScore())));
        setScoreColor(distractionScoreContainer, trip.getDistractionScore());

        totalScore.setText(String.valueOf(Math.round(trip.getScore())));
        setScoreColor(totalScoreContainer, trip.getScore());

        mapFragment.init(new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {
                if (error == OnEngineInitListener.Error.NONE) {
                    map = mapFragment.getMap();
                    mapFragment.getMapGesture().addOnGestureListener(new MarkerSelectionListener(), 25 ,false);
                    map.setCartoMarkersVisible(false);
                    map.setSafetySpotsVisible(false);
                    EnumSet<Map.LayerCategory> set = EnumSet.of(
                            Map.LayerCategory.POI_LABEL,
                            Map.LayerCategory.LABEL_STREET_CATEGORY_0,
                            Map.LayerCategory.LABEL_STREET_CATEGORY_1,
                            Map.LayerCategory.LABEL_STREET_CATEGORY_2,
                            Map.LayerCategory.LABEL_STREET_CATEGORY_3,
                            Map.LayerCategory.LABEL_STREET_CATEGORY_4);

                    map.setVisibleLayers(set, false);

                    if(trip.isSerialized()){
                        loadRoute();
                    }
                    else {
                        createRoute();
                    }

                    MapMarker startMarker = new MapMarker();
                    startMarker.setCoordinate(trip.getRoute().get(0).toGeoCoord());

                    MapMarker endMarker = new MapMarker();
                    endMarker.setCoordinate(trip.getRoute().get(trip.getRoute().size()-1).toGeoCoord());

                    map.addMapObject(startMarker);
                    map.addMapObject(endMarker);


                } else {
                    System.out.println("ERROR: Cannot initialize Map Fragment");
                }
            }
        });

    }

    private class MarkerSelectionListener implements MapGesture.OnGestureListener {

        @Override
        public void onPanStart() {

        }

        @Override
        public void onPanEnd() {

        }

        @Override
        public void onMultiFingerManipulationStart() {

        }

        @Override
        public void onMultiFingerManipulationEnd() {

        }

        @Override
        public boolean onMapObjectsSelected(List<ViewObject> list) {
            for (ViewObject viewObj : list) {
                if (viewObj.getBaseType() == ViewObject.Type.USER_OBJECT) {
                    if (((MapObject)viewObj).getType() == MapObject.Type.ROUTE  &&
                            ((MapRoute)viewObj).getColor() != Color.GREEN) {
                        View view = mapFragment.getView();
                        LayoutInflater inflater = (LayoutInflater)
                                view.getContext().getSystemService(view.getContext().LAYOUT_INFLATER_SERVICE);

                        View popupView =  inflater.inflate(R.layout.violation_popup_window, null);
                        final PopupWindow popupWindow = new PopupWindow(popupView,
                                LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT,
                                true);

                        popupWindow.showAtLocation(view, Gravity.CENTER, 0,-200);

                        //TextView title = popupView.findViewById(R.id.lbl_violationTitle);
                        TextView speedLimit = popupView.findViewById(R.id.lbl_speedLimit);
                        TextView aveSpeed = popupView.findViewById(R.id.lbl_aveSpeed);
                        TextView maxSpeed = popupView.findViewById(R.id.lbl_maxSpeed);

                        SpeedError se = violationMarkerMap.get(viewObj);

                        speedLimit.setText(getResources().getString(R.string.text_speed_limit) + se.getSpeedLimit() + getResources().getString(R.string.text_kmh));
                        aveSpeed.setText(getResources().getString(R.string.text_your_average_speed) + (Math.round(se.getAverageSpeed()*100.0)/100.0) + getResources().getString(R.string.text_kmh));
                        maxSpeed.setText(getResources().getString(R.string.text_your_max_speed) + se.getMaxSpeed() + getResources().getString(R.string.text_kmh));


                        popupView.setOnTouchListener(new View.OnTouchListener(){

                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                popupWindow.dismiss();
                                return true;
                            }
                        });
                        break;
                       // ((MapRoute)viewObj).setColor(Color.BLUE);
                    }
                }
            }

            return false;
        }



        @Override
        public boolean onTapEvent(PointF pointF) {
            return false;
        }

        @Override
        public boolean onDoubleTapEvent(PointF pointF) {
            return false;
        }

        @Override
        public void onPinchLocked() {

        }

        @Override
        public boolean onPinchZoomEvent(float v, PointF pointF) {
            return false;
        }

        @Override
        public void onRotateLocked() {

        }

        @Override
        public boolean onRotateEvent(float v) {
            return false;
        }

        @Override
        public boolean onTiltEvent(float v) {
            return false;
        }

        @Override
        public boolean onLongPressEvent(PointF pointF) {
            return false;
        }

        @Override
        public void onLongPressRelease() {

        }

        @Override
        public boolean onTwoFingerTapEvent(PointF pointF) {
            return false;
        }
    }
}


