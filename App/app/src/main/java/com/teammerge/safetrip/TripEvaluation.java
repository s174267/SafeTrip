package com.teammerge.safetrip;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.zetterstrom.com.forecast.ForecastClient;
import android.zetterstrom.com.forecast.ForecastConfiguration;
import android.util.Log;
import android.zetterstrom.com.forecast.models.Forecast;
import android.zetterstrom.com.forecast.models.Unit;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.routing.CoreRouter;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RouteResult;
import com.here.android.mpa.routing.RoutingError;
import com.here.android.mpa.search.ErrorCode;
import com.here.android.mpa.search.Location;
import com.here.android.mpa.search.ResultListener;
import com.here.android.mpa.search.ReverseGeocodeRequest;

import org.apache.commons.math3.geometry.euclidean.threed.Plane;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.stat.correlation.Covariance;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TripEvaluation extends Worker {

    private final String API_KEY = "d6fa69abedb2248bf90b541b9ce712f1";
    final int END_CUT_OFF = 100;
    public Trip trip = new Trip();

    boolean startFetch, endFetch, routeFetch = false;
    Context appContext;
    ArrayBlockingQueue<String> taskQueue = new ArrayBlockingQueue<>(3);
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseStorage storage = FirebaseStorage.getInstance();

    FileOutputStream fileOutputStream;
    int length;
    Uri logFile;
    Uri vizFile;
    long[] time;
    double[] speed;
    double[] speedLimit;
    double[] xAcc;
    double[] yAcc;
    double[] zAcc;

    int[] screenOn;

    double[] xJerk;
    double[] yJerk;
    double[] zJerk;

    double[] jerks;

    double[] lat;
    double[] lng;

    Vector3D eigenvector;
    Plane xyPlane;

    File cacheDir;

    int numberOfForecast;
    int forecastsFetched = 0;
    ArrayBlockingQueue<Weather> weatherForecastsQueue;



    public TripEvaluation(@NonNull Context context, WorkerParameters workerParams, int lengthOfFile, Uri file){
        super(context, workerParams);

        this.length = lengthOfFile - END_CUT_OFF;
        this.logFile = file;
        time = new long[length];
        speed = new double[length];
        speedLimit = new double[length];
        xAcc = new double[length];
        yAcc = new double[length];
        zAcc = new double[length];
        xJerk = new double[length];
        yJerk = new double[length];
        zJerk = new double[length];
        lat = new double[length];
        lng = new double[length];
        jerks = new double[length];
        screenOn = new int[length];


        this.appContext = context;

    }


    public TripEvaluation(@NonNull Context context, @NonNull WorkerParameters workerParams){
        super(context, workerParams);
        Data data = this.getInputData();

        Map<String, Object> dataMap = data.getKeyValueMap();

        Map<String, Object> fileDataMap = MainActivity.getInstance().getFileWriteOptions();

        this.length = (int) dataMap.get("LENGTH_OF_FILE") - END_CUT_OFF;
        this.logFile = (Uri) fileDataMap.get("DATA_FILE");
        time = new long[length];
        speed = new double[length];
        speedLimit = new double[length];
        xAcc = new double[length];
        yAcc = new double[length];
        zAcc = new double[length];
        xJerk = new double[length];
        yJerk = new double[length];
        zJerk = new double[length];
        lat = new double[length];
        lng = new double[length];
        jerks = new double[length];
        screenOn = new int[length];

        this.cacheDir = (File) fileDataMap.get("CACHEDIR");
        this.fileOutputStream = (FileOutputStream) fileDataMap.get("FILE_OUTPUT_STREAM");
        this.vizFile = (Uri) fileDataMap.get("VIZFILE");
        this.appContext = context;

    }

    public void readData() {
        try {
            BufferedReader csvReader = new BufferedReader(new FileReader(this.logFile.getPath()));
            String row;
            String autoHeader = csvReader.readLine();

            if (autoHeader.equals("auto")){
                trip.setApproved(false);
            } else {
                trip.setApproved(true);
            }
            csvReader.readLine();

            for(int i = 0; i < END_CUT_OFF/2; i++){
                csvReader.readLine();
            }
            for (int i = 0; i < length; i++) {
                row = csvReader.readLine();
                String[] data = row.split(",");
                if (data[0].equals("time")) {
                } else {
                    speed[i] = Double.parseDouble(data[1]);
                    speedLimit[i] = Double.parseDouble(data[2]);
                    lat[i] = Double.parseDouble(data[3]);
                    lng[i] = Double.parseDouble(data[4]);
                    xAcc[i] = Double.parseDouble(data[5]);
                    yAcc[i] = Double.parseDouble(data[6]);
                    zAcc[i] = Double.parseDouble(data[7]);
                    time[i] = Long.valueOf(data[0]).longValue();
                    screenOn[i] = Integer.valueOf(data[9].trim());

                    if((i % 500) == 0){
                        trip.getRoute().add(new LocationCoordinates(lat[i],lng[i]));
                    }
                }
            }
            if(length>0){
                trip.getRoute().add(new LocationCoordinates(lat[length-1], lng[length-1]));
            }
            csvReader.close();

        } catch (Error e){
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    public void dataLogForVisualization(){
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
            outputStreamWriter.write("time, jerks, speed, speed_limit, lat, lng \n");
            for (int i = 0; i < length ; i++) {
                outputStreamWriter.append(
                        (time[i]-time[0]) +", " +
                        jerks[i] + ", " +
                        speed[i] + ", " +
                        speedLimit[i] + ", " +
                        lat[i] + ", " +
                        lng[i] +
                        "\n");

            }
            outputStreamWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference();
        String filename = mAuth.getCurrentUser().getEmail() + "/logData/" + new Date().toString() +"viz.txt";
        StorageReference vizData = storageRef.child(filename);
        vizData.putFile(vizFile);
    }
    public Trip runTripAnalysis(){
        readData();

        trip.setStartTime(new Date(time[0]));
        trip.setEndTime(new Date(time[length-1]));
        trip.generateId();
        calculateJerks();
        calculatePCA();
        projectJerks();
        calculateJerkScore();
        calculateDistractionScore();
        analyzeSpeedErrors();
        analyzeSpeedStats();
        getWeather();
        getStartCity();
        getEndCity();
        createRoute();

        try {
            while(weatherForecastsQueue.take() != null){
                if(forecastsFetched >= numberOfForecast){
                    break;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        analyzeContextViolations();
        calculateContextScore();

        calculateSpeedScore();

        calculateScore();
        dataLogForVisualization();

        try {
            while(taskQueue.take() != null){
                if(startFetch && endFetch && routeFetch){
                    break;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        startPostTripActivity();
        updateUserScore();

        return this.trip;
    }

    public void startPostTripActivity(){
        db.collection(mAuth.getCurrentUser().getEmail()+ "-trips").document(trip.getId())
                .set(trip, SetOptions.merge())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        MainActivity.getInstance().resetMainPage();
                        Intent intent = new Intent(appContext, TripDetailsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        Gson gson = new Gson();
                        intent.putExtra("trip", gson.toJson(trip));
                        appContext.startActivity(intent);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("FAIL", "Error adding document", e);
                    }
                });


    }

    public void calculateDistractionScore(){
        double phoneOn = 0;
        double phoneOff = 0;
        boolean firstOff = false;
        int numberOfScreenOns = 0;
        boolean activePhoneOn = false;
        for (int i = 0; i < length; i++) {
            if(screenOn[i] == 1 && firstOff == true){
                phoneOn++;
                if(activePhoneOn == false){
                    numberOfScreenOns++;
                    activePhoneOn = true;
                }
            }
            else if(screenOn[i] == 0){
                firstOff = true;
                phoneOff++;
                activePhoneOn = false;
            }
        }
        double lastPhoneOn = 0;
        for (int i = length-1; i >= 0 ; i--) {
            if(screenOn[i] == 1){
                lastPhoneOn++;
            }
            else {
                break;
            }
        }
        phoneOn = phoneOn-lastPhoneOn;
        double value = 0;
        if(phoneOff == 0){
            numberOfScreenOns++;
        }
        else {
            value = phoneOn/(phoneOn+phoneOff);
        }

        double distractionScore = (((0.15 - value)*(100.0-00.0)) / (0.15-0.0)) + 00.0;
        if(distractionScore>100){
            distractionScore = 100;
        }
        else if (distractionScore < 0){
            distractionScore = 0;
        }

        trip.setNumberOfScreenOns(numberOfScreenOns);
        trip.setPercentageScreenOn(value*100);
        trip.setDistractionScore(distractionScore);
        
    }


    public void calculateSpeedScore(){
        double score = 100;
        for (SpeedError sError : trip.getSpeedViolations()) {
            long tripDuration = trip.getEndTime().getTime() - trip.getStartTime().getTime();

            double timeFactor = sError.getDuration() / (tripDuration / 10.0) ;
            double speedFactor = (Math.pow((sError.getAverageSpeed() / sError.getSpeedLimit()), 2.2));
            double normalizedSpeedFactor = speedFactor / 5.0;

            if(normalizedSpeedFactor > 1){
                normalizedSpeedFactor = 1;
            }
            else if(normalizedSpeedFactor < 0){
                normalizedSpeedFactor = 0;
            }
            sError.setScoreImpact(((timeFactor * normalizedSpeedFactor) * 100));

            if((timeFactor*normalizedSpeedFactor) > -500 && (timeFactor*normalizedSpeedFactor) < 500){ //Sanity Check
                score = score - ((timeFactor * normalizedSpeedFactor) * 100);

            }


        }
        if(score < 0){
            trip.setSpeedScore(0);
        }
        else {
            trip.setSpeedScore(score);
        }
    }


    public void calculateScore(){
        boolean includeWeather = false;
        for (Weather weather: trip.getWeather()) {
            if(weather.getWeatherCondition() != Weather.WeatherConditions.CLEAR_AND_DRY){
                includeWeather = true;
            }
        }
        double score = 0;
        if(includeWeather){
            score = (trip.getJerkScore() + trip.getContextScore() + trip.getSpeedScore() + trip.getDistractionScore())/4.0;

        }
        else {
            score = (trip.getJerkScore() + trip.getSpeedScore() + trip.getDistractionScore())/3.0;
        }

        trip.setScore(Math.round(score));
    }

    public void analyzeSpeedStats(){
        double speedSum = 0;
        double speedLimitSum = 0;
        double movingMeasurements = 0;
        for (int i = 0; i <length ; i++) {
            if(speed[i] > 5){
                speedSum = speedSum + speed[i];
                speedLimitSum = speedLimitSum + speedLimit[i];
                movingMeasurements++;
            }
        }
        if(speedSum > 0 && movingMeasurements > 0){
            trip.setAverageSpeed(Math.round(speedSum/movingMeasurements));
        }
        if(speedSum > 0 && speedLimitSum > 0){
            trip.setRelativeSpeedAverage(speedSum/speedLimitSum);
        }
    }

    private void endSpeedError(SpeedError se, int index, int indexStart){
        se.setEndPoint(new LocationCoordinates(lat[index], lng[index]));
        se.setDuration(time[index] - se.getTime().getTime());
        calculateSpeedAverageAndMax(indexStart, index, se);
        trip.getSpeedViolations().add(se);
    }
    public void analyzeSpeedErrors(){
        boolean activeError = false;
        int activeErrorStart = 0;
        int activeErrorLength = 0;
        int noErrorLength = 0;
        SpeedError currentError = new SpeedError();
        for (int i = 0; i < length ; i++) {
            if(activeError){
                if(speedLimit[i] != currentError.getSpeedLimit()){
                    if(activeErrorLength > 20){
                        endSpeedError(currentError, i, activeErrorStart);
                        activeError = false;
                        activeErrorLength = 0;
                    }
                    else {
                        activeError = false;
                        activeErrorLength = 0;
                    }
                }
                else if(speed[i] < speedLimit[i] + 3){
                    noErrorLength++;
                    if(noErrorLength > 10){
                        if(activeErrorLength > 20){
                            endSpeedError(currentError, i, activeErrorStart);
                            activeError = false;
                            activeErrorLength = 0;
                        }
                        else {
                            activeError = false;
                            activeErrorLength = 0;
                        }
                    }
                }
                else{
                    noErrorLength = 0;
                    activeErrorLength++;
                }
            }
            else {
                if(speed[i] > speedLimit[i] + 3 && speed[i] < 100000 && speedLimit[i] > 0){
                    currentError = new SpeedError();
                    currentError.setStartPoint(new LocationCoordinates(lat[i], lng[i]));
                    currentError.setTime(new Date(time[i]));
                    currentError.setSpeedLimit(speedLimit[i]);
                    activeError = true;
                    activeErrorStart = i;
                }
            }
        }
    }

    public void calculateSpeedAverageAndMax(int startIndex, int endIndex, DriverError error){
        double sumSpeed = 0;
        double maxSpeed = -9000;
        for (int i = startIndex; i <= endIndex ; i++) {
            sumSpeed = sumSpeed + speed[i];
            if(speed[i] > maxSpeed){
                maxSpeed = speed[i];
            }
        }
        error.setMaxSpeed(maxSpeed);
        error.setAverageSpeed(sumSpeed/(endIndex-startIndex+1));

    }
    public void calculateJerks(){
        for (int i = 1; i < length-1; i++) {

            if (time[i+1] == time[i-1]) {
                continue;
            }

            xJerk[i] = (xAcc[i+1] - xAcc[i-1]) / (time[i+1] - time[i-1]);
            yJerk[i] = (yAcc[i+1] - yAcc[i-1]) / (time[i+1] - time[i-1]);
            zJerk[i] = (zAcc[i+1] - zAcc[i-1]) / (time[i+1] - time[i-1]);
        }
    }

    public void calculatePCA(){
        double[][] accelerationArray = new double[][] {
                xJerk,
                yJerk,
                zJerk};
        RealMatrix realMatrix = MatrixUtils.createRealMatrix(accelerationArray);
        realMatrix = realMatrix.transpose();
        Covariance covariance = new Covariance(realMatrix);
        RealMatrix covarianceMatrix = covariance.getCovarianceMatrix();
        EigenDecomposition ed = new EigenDecomposition(covarianceMatrix);

        int maxEigen = -1;
        double maxEigenValue = -9999999;

        for (int i = 0; i < 3; i++) {
            if(ed.getRealEigenvalues()[i] > maxEigenValue){
                maxEigenValue = ed.getRealEigenvalues()[i];
                maxEigen = i;

            }
        }
        RealVector eigen = ed.getEigenvector(maxEigen);
        this.eigenvector = new Vector3D(eigen.getEntry(0), eigen.getEntry(1), eigen.getEntry(2));
        this.xyPlane = new Plane(this.eigenvector, 0.000001);

    }

    public void projectJerks(){
        for (int i = 0; i < length; i++) {
            Vector3D measuredPoint = new Vector3D(xAcc[i], yAcc[i], zAcc[i]);
            Vector3D projectedPoint = (Vector3D) this.xyPlane.project(measuredPoint);
            if(screenOn[i] == 0){
                jerks[i] = getVectorLength(projectedPoint);
            }
            else {
                jerks[i] = 0;
            }
        }
    }

    public void calculateJerkScore(){

        Percentile percentile = new Percentile();
        percentile.setQuantile(99);
        double jerk99 = percentile.evaluate(jerks, 99);
        double jerkScore = (((2.6 - jerk99)*(100.0-00.0)) / (2.6-1.0)) + 00.0;

        if(jerkScore > 100){
            jerkScore = 100;
        }
        else if(jerkScore<0){
            jerkScore = 0;
        }
        trip.setJerkScore(jerkScore);
    }

    public double getVectorLength(Vector3D vector){
        return Math.sqrt(Math.pow(vector.getX(), 2) + Math.pow(vector.getY(), 2) + Math.pow(vector.getZ(), 2));
    }
    private int analyzeContext(int startIndex, ContextViolation violation){
        int violationLength = 0;
        int noViolation = 0;
        for (int i = startIndex; i < length; i++) {
            if(speed[i] < violation.getRecommendedSpeedLimit() + 3){
                noViolation++;
                if(noViolation > 10){
                    if(violationLength > 20){
                        violation.setEndPoint(new LocationCoordinates(lat[i], lng[i]));
                        violation.setDuration(time[i] - violation.getTime().getTime());
                        calculateSpeedAverageAndMax(startIndex, i, violation);
                        trip.getContextViolations().add(violation);
                        return i;
                    }
                    else {
                        return i;
                    }
                }
            }
            else {
                violationLength++;
            }
        }
        return length;

    }

    public void analyzeContextViolations(){
        for (int i = 0; i < length ; i++) {
            if(speedLimit[i] > 0){
                if(speedLimit[i] < 61){

                }
                else if(speedLimit[i] >= 61 && speedLimit[i] < 81){
                    if(getWeatherFromIndex(i).getWeatherCondition() == Weather.WeatherConditions.LIGHT_RAIN_AND_LIGHT_SNOW || getWeatherFromIndex(i).getWeatherCondition() == Weather.WeatherConditions.FREEZING){
                        if(speed[i] > (speedLimit[i] * 0.9167)+3){{
                            ContextViolation violation = new ContextViolation();
                            violation.setRecommendedSpeedLimit(speedLimit[i] * 0.9167);
                            violation.setWeather(Weather.WeatherConditions.LIGHT_RAIN_AND_LIGHT_SNOW);
                            violation.setStartPoint(new LocationCoordinates(lat[i], lng[i]));
                            violation.setTime(new Date(time[i]));
                            i = analyzeContext(i, violation);
                        }}
                    }
                    else if(getWeatherFromIndex(i).getWeatherCondition() == Weather.WeatherConditions.HEAVY_RAIN){
                        if(speed[i] > (speedLimit[i] * 0.8333)+3){{
                            ContextViolation violation = new ContextViolation();
                            violation.setRecommendedSpeedLimit(speedLimit[i] * 0.8333);
                            violation.setWeather(Weather.WeatherConditions.HEAVY_RAIN);
                            violation.setStartPoint(new LocationCoordinates(lat[i], lng[i]));
                            violation.setTime(new Date(time[i]));
                            i = analyzeContext(i, violation);
                        }}
                    }
                    else if(getWeatherFromIndex(i).getWeatherCondition() == Weather.WeatherConditions.HEAVY_SNOW){
                        if(speed[i] > (speedLimit[i] * 0.6)+3){{
                            ContextViolation violation = new ContextViolation();
                            violation.setRecommendedSpeedLimit(speedLimit[i] * 0.6);
                            violation.setWeather(Weather.WeatherConditions.HEAVY_SNOW);
                            violation.setStartPoint(new LocationCoordinates(lat[i], lng[i]));
                            violation.setTime(new Date(time[i]));
                            i = analyzeContext(i, violation);
                        }}
                    }
                }
                else if(speedLimit[i] >= 81){
                    if(getWeatherFromIndex(i).getWeatherCondition() == Weather.WeatherConditions.LIGHT_RAIN_AND_LIGHT_SNOW){
                        if(speed[i] > (speedLimit[i] * 0.9167)+3){{
                            ContextViolation violation = new ContextViolation();
                            violation.setRecommendedSpeedLimit(speedLimit[i] * 0.9167);
                            violation.setWeather(Weather.WeatherConditions.LIGHT_RAIN_AND_LIGHT_SNOW);
                            violation.setStartPoint(new LocationCoordinates(lat[i], lng[i]));
                            violation.setTime(new Date(time[i]));
                            i = analyzeContext(i, violation);
                        }}
                    }
                    else if(getWeatherFromIndex(i).getWeatherCondition() == Weather.WeatherConditions.HEAVY_RAIN){
                        if(speed[i] > (speedLimit[i] * 0.8333)+3){{
                            ContextViolation violation = new ContextViolation();
                            violation.setRecommendedSpeedLimit(speedLimit[i] * 0.8333);
                            violation.setWeather(Weather.WeatherConditions.HEAVY_RAIN);
                            violation.setStartPoint(new LocationCoordinates(lat[i], lng[i]));
                            violation.setTime(new Date(time[i]));
                            i = analyzeContext(i, violation);
                        }}
                    }
                    else if(getWeatherFromIndex(i).getWeatherCondition() == Weather.WeatherConditions.HEAVY_SNOW){
                        if(speed[i] > (speedLimit[i] * 0.6)+3){{
                            ContextViolation violation = new ContextViolation();
                            violation.setRecommendedSpeedLimit(speedLimit[i] * 0.6);
                            violation.setWeather(Weather.WeatherConditions.HEAVY_SNOW);
                            violation.setStartPoint(new LocationCoordinates(lat[i], lng[i]));
                            violation.setTime(new Date(time[i]));
                            i = analyzeContext(i, violation);
                        }}
                    }
                }
            }

        }
    }

    public void calculateContextScore(){
        double score = 100;
        for (ContextViolation violation: trip.getContextViolations()) {
            long tripDuration = trip.getEndTime().getTime() - trip.getStartTime().getTime();

            double timeFactor = violation.getDuration() / (tripDuration / 10.0) ;
            double speedFactor = (Math.pow((violation.getAverageSpeed() / violation.getRecommendedSpeedLimit()), 2.2));
            double normalizedSpeedFactor = speedFactor / 5.0;

            if(normalizedSpeedFactor > 1){
                normalizedSpeedFactor = 1;
            }
            else if(normalizedSpeedFactor < 0){
                normalizedSpeedFactor = 0;
            }
            violation.setScoreImpact(((timeFactor * normalizedSpeedFactor) * 100));

            if((timeFactor*normalizedSpeedFactor) > -500 && (timeFactor*normalizedSpeedFactor) < 500){ //Sanity Check
                score = score - ((timeFactor * normalizedSpeedFactor) * 100);

            }
        }
        if(score<0){
            score = 0;
        }
        else if(score > 100){
            score = 100;
        }
        trip.setContextScore(score);
    }



    public Weather getWeatherFromIndex(int index){
        Weather closestWeather = null;
        int closestDist = 999999;
        for (Weather weather: trip.getWeather()) {
            int dist = Math.abs(weather.getIndex()-index);
            if(dist < closestDist){
                closestDist = dist;
                closestWeather = weather;
            }
        }
        return closestWeather;
    }
    public void getWeather(){

        double tripDuration = (trip.getEndTime().getTime() - trip.getStartTime().getTime())/1000;

        if(tripDuration < 900) {
            numberOfForecast = 1;
        } else {
            numberOfForecast = (int) (tripDuration / 900.0); // 900 s = 15 min
        }
        weatherForecastsQueue = new ArrayBlockingQueue<>(numberOfForecast);

        ForecastConfiguration configuration =
                new ForecastConfiguration.Builder(API_KEY)
                        .setCacheDirectory(cacheDir)
                        .build();

        int interval = length/numberOfForecast;

        for (int i = 0; i < numberOfForecast; i++) {

            final int number = interval/2 + i*interval;
            final int weatherTime = Math.toIntExact(time[number]/1000);

            ForecastClient.create(configuration);
            Unit unit =  Unit.SI;


            ForecastClient.getInstance()
                    .getForecast(lat[number], lng[number], weatherTime, null, unit, null, false, new Callback<Forecast>() {
                        @Override
                        public void onResponse(Call<Forecast> forecastCall, Response<Forecast> response) {
                            if (response.isSuccessful()) {
                                Weather weather = new Weather(response.body(), number, weatherTime);
                                forecastsFetched++;
                                weatherForecastsQueue.add(weather);
                                trip.getWeather().add(weather);

                            }
                        }

                        @Override
                        public void onFailure(Call<Forecast> forecastCall, Throwable t) {
                            t.printStackTrace();
                            weatherForecastsQueue.add(null);
                        }
                    });
        }

    }

    public void getStartCity(){
        final GeoCoordinate startPos = trip.getRoute().get(0).toGeoCoord();
        ReverseGeocodeRequest requestStart = new ReverseGeocodeRequest(startPos);
        ResultListener<Location> startListener = new ResultListener<Location>() {
            @Override
            public void onCompleted(Location location, ErrorCode errorCode) {
                if(errorCode == ErrorCode.NONE){
                    if(location == null) {
                        trip.setStartCity(appContext.getString(R.string.text_unknown_city));
                    } else {
                        trip.setStartCity(location.getAddress().getCity());
                    }
                    
                    startFetch = true;
                    taskQueue.add("Start City");

                }
            }
        };
        requestStart.execute(startListener);
    }

    public void getEndCity(){
        GeoCoordinate endPos = trip.getRoute().get(trip.getRoute().size()-1).toGeoCoord();
        ReverseGeocodeRequest requestEnd = new ReverseGeocodeRequest(endPos);
        ResultListener<Location> endListener = new ResultListener<Location>() {
            @Override
            public void onCompleted(Location location, ErrorCode errorCode) {
                if(errorCode == ErrorCode.NONE){
                    if(location == null) {
                        trip.setEndCity(appContext.getString(R.string.text_unknown_city));
                    } else {
                        trip.setEndCity(location.getAddress().getCity());
                    }

                    endFetch = true;
                    taskQueue.add("End City");

                }
            }
        };

        requestEnd.execute(endListener);
    }
    public void createRoute(){
        ArrayList<GeoCoordinate> points = new ArrayList<>();
        for(LocationCoordinates coord: trip.getRoute()){
            points.add(coord.toGeoCoord());
        }

        RouteOptions routeOptions = new RouteOptions();
        routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
        routeOptions.setRouteType(RouteOptions.Type.SHORTEST);

        CoreRouter coreRouter = new CoreRouter();
        coreRouter.calculateRoute(points, routeOptions, new CoreRouter.Listener() {
            @Override
            public void onCalculateRouteFinished(List<RouteResult> list, RoutingError routingError) {
                if (routingError == RoutingError.NONE) {
                    RouteResult result = list.get(0);
                    Route resultRoute = result.getRoute();
                    trip.setLength(resultRoute.getLength()/1000.0);
                    Route.serializeAsync(resultRoute, new Route.SerializationCallback() {
                        @Override
                        public void onSerializationComplete(Route.SerializationResult serializationResult) {
                            byte[] mapSerialized = serializationResult.data;
                            StorageReference storageRef = storage.getReference();
                            String filename = mAuth.getCurrentUser().getEmail() + "/routes/"+ trip.getId() + "/" + "route.route";
                            StorageReference logData = storageRef.child(filename);
                            logData.putBytes(mapSerialized);
                        }
                    });
                    markSpeedErrors();
                }



            }
            @Override
            public void onProgress(int i) {
            }
        });
    }

    public void markSpeedErrors(){
        RouteOptions routeOptions = new RouteOptions();
        routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
        routeOptions.setRouteType(RouteOptions.Type.SHORTEST);

        for(final SpeedError se : trip.getSpeedViolations()){
            ArrayList<GeoCoordinate> points = new ArrayList<>();
            points.add(se.getStartPoint().toGeoCoord());
            points.add(se.getEndPoint().toGeoCoord());

            CoreRouter coreRouter = new CoreRouter();
            coreRouter.calculateRoute(points, routeOptions, new CoreRouter.Listener() {
                @Override
                public void onCalculateRouteFinished(List<RouteResult> list, RoutingError routingError) {
                    if (routingError == RoutingError.NONE) {
                        RouteResult result = list.get(0);
                        Route resultRoute = result.getRoute();

                        Route.serializeAsync(resultRoute, new Route.SerializationCallback() {
                            @Override
                            public void onSerializationComplete(Route.SerializationResult serializationResult) {
                                byte[] mapSerialized = serializationResult.data;
                                StorageReference storageRef = storage.getReference();
                                String filename = mAuth.getCurrentUser().getEmail() + "/routes/"+trip.getId() +"/" + trip.getSpeedViolations().indexOf(se) +".route";
                                StorageReference logData = storageRef.child(filename);
                                logData.putBytes(mapSerialized);
                            }
                        });

                    }
                }
                @Override
                public void onProgress(int i) {
                }
            });

        }

        trip.setSerialized(true);
        db.collection(mAuth.getCurrentUser().getEmail()+ "-trips").document(trip.getId())
                .set(trip, SetOptions.merge())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.w("Succ","SUCC");
                        taskQueue.add("Route");
                        routeFetch = true;
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        taskQueue.add("Route");
                        routeFetch = true;
                        Log.w("FAIL", "Error adding document", e);
                    }
                });

    }



    public void updateUserScore(){
        db.collection(mAuth.getCurrentUser().getEmail()+ "-trips")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        ArrayList<Trip> userTrips = new ArrayList<>();
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Trip trip = document.toObject(Trip.class);
                                userTrips.add(trip);
                            }
                            double score = 0;
                            double totalDuration = 0;

                            Collections.sort(userTrips);
                            Collections.reverse(userTrips);

                            int counter = 0;
                            for(Trip trip : userTrips){
                                if (trip.isApproved()) {
                                    score = score + (trip.getScore() * trip.getCalcDuration());
                                    totalDuration = totalDuration + trip.getCalcDuration();
                                    counter++;
                                }

                                if (counter >= 10) {
                                    break;
                                }
                            }


                            // Update totalScore in firebase to new value
                            db.collection("users").document(mAuth.getCurrentUser().getEmail())
                                    .update("totalScore", Math.round(score/totalDuration))
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Log.d("test_TAG", "DocumentSnapshot successfully updated!");
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.w("test_TAG", "Error updating document", e);
                                        }
                                    });


                        } else {
                            // Update totalScore in firebase to 0
                            db.collection("users").document(mAuth.getCurrentUser().getEmail())
                                    .update("totalScore", 0)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Log.d("test_TAG", "DocumentSnapshot successfully updated!");
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.w("test_TAG", "Error updating document", e);
                                        }
                                    });
                        }
                    }
                });
    }


    @NonNull
    @Override
    public Result doWork() {
        this.runTripAnalysis();
        return Result.success();
    }

}
