package com.teammerge.safetrip;

public class User {
    private String displayName;
    private String email;
    private double averageSpeedScore;
    private double averageAggressiveScore;
    private double averageWeatherScore;
    private double averageDistractionScore;
    private double totalDistanceTraveled;
    private double totalTimeTraveled;
    private double totalScore;

    public User() {
    }

    public User(String displayName, String email) {
        this.displayName = displayName;
        this.email = email;
    }


    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getAverageSpeedScore() {
        return averageSpeedScore;
    }

    public void setAverageSpeedScore(double averageSpeedScore) {
        this.averageSpeedScore = averageSpeedScore;
    }

    public double getAverageAggressiveScore() {
        return averageAggressiveScore;
    }

    public void setAverageAggressiveScore(double averageAggressiveScore) {
        this.averageAggressiveScore = averageAggressiveScore;
    }

    public double getAverageWeatherScore() {
        return averageWeatherScore;
    }

    public void setAverageWeatherScore(double averageWeatherScore) {
        this.averageWeatherScore = averageWeatherScore;
    }

    public double getAverageDistractionScore() {
        return averageDistractionScore;
    }

    public void setAverageDistractionScore(double averageDistractionScore) {
        this.averageDistractionScore = averageDistractionScore;
    }

    public double getTotalDistanceTraveled() {
        return totalDistanceTraveled;
    }

    public void setTotalDistanceTraveled(double totalDistanceTraveled) {
        this.totalDistanceTraveled = totalDistanceTraveled;
    }

    public double getTotalTimeTraveled() {
        return totalTimeTraveled;
    }

    public void setTotalTimeTraveled(double totalTimeTraveled) {
        this.totalTimeTraveled = totalTimeTraveled;
    }

    public double getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(double totalScore) {
        this.totalScore = totalScore;
    }
}
