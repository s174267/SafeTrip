package com.teammerge.safetrip;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class UserStatisticsActivity extends AppCompatActivity {

    FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;

    LinearLayout todayLinearLayout;

    TextView overallDrivingDuration;
    TextView todayDrivingDuration;

    TextView overallDrivingDistance;
    TextView todayDrivingDistance;

    TextView overallJerkScore;
    TextView todayJerkScore;

    TextView overallWeatherScore;
    TextView todayWeatherScore;

    TextView overallSpeedScore;
    TextView todaySpeedScore;

    TextView overallDistractionScore;
    TextView todayDistractionScore;

    TextView overallScreenOnTime;
    TextView todayScreenOnTime;

    int overallDuration;
    int todayDistance;

    int overallDistance;
    int todayDuration;

    int overallJerk;
    int todayJerk;

    int overallWeather;
    int todayWeather;

    int overallSpeed;
    int todaySpeed;

    int overallScreenOn;
    int todayScreenOn;

    int overallDistraction;
    int todayDistraction;

    double overallCounter;
    double todayCounter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_statistics);



        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        db = FirebaseFirestore.getInstance();

        todayLinearLayout = findViewById(R.id.today_ll);

        overallDrivingDuration = findViewById(R.id.overallDrivingTimeValue);
        todayDrivingDuration = findViewById(R.id.todayDrivingTimeValue);

        todayDrivingDistance = findViewById(R.id.todayDrivingDistanceValue);
        overallDrivingDistance = findViewById(R.id.overallDrivingDistanceValue);

        overallJerkScore = findViewById(R.id.overallJerkScoreValue);
        todayJerkScore = findViewById(R.id.todayJerkScoreValue);

        overallWeatherScore = findViewById(R.id.overallWeatherScoreValue);
        todayWeatherScore = findViewById(R.id.todayWeatherScoreValue);

        overallSpeedScore = findViewById(R.id.overallSpeedScoreValue);
        todaySpeedScore = findViewById(R.id.todaySpeedScoreValue);

        overallDistractionScore = findViewById(R.id.overallDistractionScoreValue);
        todayDistractionScore = findViewById(R.id.todayDistractionScoreValue);

        overallScreenOnTime = findViewById(R.id.overallScreenOnValue);
        todayScreenOnTime = findViewById(R.id.todayScreenOnValue);


        overallDistance = 0;
        todayDistance = 0;

        overallDuration = 0;
        todayDuration = 0;

        overallJerk = 0;
        todayJerk = 0;

        overallWeather = 0;
        todayWeather = 0;

        overallSpeed = 0;
        todaySpeed = 0;

        overallDistraction = 0;
        todayDistraction = 0;

        overallScreenOn = 0;
        todayScreenOn = 0;

        overallCounter = 0;
        todayCounter = 0;

        db.collection(currentUser.getEmail()+"-trips")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException e) {
                        for (QueryDocumentSnapshot doc : value) {
                            Trip trip = doc.toObject(Trip.class);

                            if (isSameDate(trip.getStartTime(), new Date())) {
                                todayDuration += trip.getCalcDuration();
                                todayDistance += trip.getLength();
                                todayJerk += trip.getJerkScore() * trip.getCalcDuration();
                                todayWeather += trip.getContextScore() * trip.getCalcDuration();
                                todaySpeed += trip.getSpeedScore() * trip.getCalcDuration();
                                todayDistraction += trip.getDistractionScore() * trip.getCalcDuration();
                                todayScreenOn += (trip.getPercentageScreenOn()/100) * trip.getCalcDuration();

                                todayCounter += trip.getCalcDuration();

                            }
                            overallDuration += trip.getCalcDuration();
                            overallDistance += trip.getLength();
                            overallJerk += trip.getJerkScore() * trip.getCalcDuration();
                            overallWeather += trip.getContextScore() * trip.getCalcDuration();
                            overallSpeed += trip.getSpeedScore() * trip.getCalcDuration();
                            overallDistraction += trip.getDistractionScore() * trip.getCalcDuration();
                            overallScreenOn += (trip.getPercentageScreenOn()/100) * trip.getCalcDuration();

                            overallCounter += trip.getCalcDuration();

                        }
                        overallDrivingDuration.setText(formatDurationToString(overallDuration));
                        overallDrivingDistance.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%d km", Math.round((overallDistance * 10.0) / 10.0)));
                        overallJerkScore.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%.0f", overallJerk / overallCounter));
                        overallWeatherScore.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%.0f", overallWeather / overallCounter));
                        overallSpeedScore.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%.0f", overallSpeed / overallCounter));
                        overallDistractionScore.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%.0f", overallDistraction / overallCounter));
                        overallScreenOnTime.setText(String.format(formatDurationToString(overallScreenOn)));


                        if(todayDuration < 1) {
                            todayLinearLayout.setVisibility(View.GONE);
                        }

                        todayDrivingDuration.setText(formatDurationToString(todayDuration));
                        todayDrivingDistance.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%d km", Math.round((todayDistance * 10.0) / 10.0)));
                        todayJerkScore.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%.0f", todayJerk / todayCounter));
                        todayWeatherScore.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%.0f", todayWeather / todayCounter));
                        todaySpeedScore.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%.0f", todaySpeed / todayCounter));
                        todayDistractionScore.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"%.0f", todayDistraction / todayCounter));
                        todayScreenOnTime.setText(String.format(formatDurationToString(todayScreenOn)));

                    }
                });

    }


    public String formatDurationToString(int duration) {
        String durationString;

        if (duration > 60*60){
            int hour =(int) (duration /(60*60));
            int minutes = (int) ((duration %(60*60))/60);

            durationString = hour + getApplication().getString(R.string.text_hour_symbol) + minutes +" min";
        }
        else {
            durationString = (duration /60) + " min";
        }

        return durationString;
    }

    public boolean isSameDate(Date date1, Date date2){
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        return fmt.format(date1).equals(fmt.format(date2));
    }



}
