package com.teammerge.safetrip;

import android.content.Context;
import android.zetterstrom.com.forecast.models.Forecast;

import androidx.annotation.NonNull;

import java.util.Date;

public class Weather {
    private double temperature;
    private double visibility;
    private double humidity;
    private double windSpeed;
    private double windBearing;
    private double precipIntensity;
    private double precipIntensityMax;
    private Date timeStamp;
    private double precipProbability;
    private String summary;
    private Date SunriseTime;
    private Date SunsetTime;
    private int index;


    enum WeatherConditions {
        CLEAR_AND_DRY {
            public String toString(){
                return "Clear and Dry";
            }
        },
        LIGHT_RAIN_AND_LIGHT_SNOW {
            public String toString(){
                return "Light rain or light snow";
            }
        },
        HEAVY_RAIN {
            public String toString(){
                return "Heavy rain";
            }
        },
        HEAVY_SNOW {
            public String toString(){
                return "Heavy snow";
            }
        },
        FREEZING {
            public String toString() {
                return super.toString();
            }
        }
    }

    public Weather(){
        //This is for the deserializer
    }
    public Weather(Forecast forecast, int index, int timeStamp) {

        try {
            this.temperature = forecast.getCurrently().getTemperature();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            this.visibility = forecast.getCurrently().getVisibility();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            this.humidity = forecast.getCurrently().getHumidity();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            this.windSpeed = forecast.getCurrently().getWindSpeed();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            this.windBearing = forecast.getCurrently().getWindBearing();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            this.precipIntensity = forecast.getCurrently().getPrecipIntensity();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            this.precipProbability = forecast.getCurrently().getPrecipProbability();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            long intermediate = (long) timeStamp * 1000;
            this.timeStamp = new Date(intermediate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            this.summary = forecast.getCurrently().getSummary();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            this.SunriseTime = forecast.getDaily().getDataPoints().get(0).getSunriseTime();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            this.SunsetTime = forecast.getDaily().getDataPoints().get(0).getSunsetTime();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            this.index = index;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public WeatherConditions getWeatherCondition(){

        if(precipIntensity >= 0.01 && precipIntensity < 1 && precipProbability > 0.25) {
            return WeatherConditions.LIGHT_RAIN_AND_LIGHT_SNOW;
        }
        else if(temperature < 3 && precipIntensity > 1 && precipProbability > 0.25) {
            return WeatherConditions.HEAVY_SNOW;
        }
        else if(precipIntensity > 1 && precipProbability > 0.5) {
            return WeatherConditions.HEAVY_RAIN;
        }
        else if(temperature < 3){
            return WeatherConditions.FREEZING;
        }
        else {
            return WeatherConditions.CLEAR_AND_DRY;
        }
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getVisibility() {
        return visibility;
    }

    public void setVisibility(double visibility) {
        this.visibility = visibility;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public double getWindBearing() {
        return windBearing;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setWindBearing(double windBearing) {
        this.windBearing = windBearing;
    }

    public double getPrecipIntensity() {
        return precipIntensity;
    }

    public void setPrecipIntensity(double precipIntensity) {
        this.precipIntensity = precipIntensity;
    }

    public double getPrecipIntensityMax() {
        return precipIntensityMax;
    }

    public void setPrecipIntensityMax(double precipIntensityMax) {
        this.precipIntensityMax = precipIntensityMax;
    }

    public double getPrecipProbability() {
        return precipProbability;
    }

    public void setPrecipProbability(double precipProbability) {
        this.precipProbability = precipProbability;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Date getSunriseTime() {
        return SunriseTime;
    }

    public void setSunriseTime(Date sunriseTime) {
        SunriseTime = sunriseTime;
    }

    public Date getSunsetTime() {
        return SunsetTime;
    }

    public void setSunsetTime(Date sunsetTime) {
        SunsetTime = sunsetTime;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
