package com.teammerge.safetrip.uiFragment.history;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.teammerge.safetrip.R;
import com.teammerge.safetrip.Trip;
import com.teammerge.safetrip.TripDetailsActivity;

public class HistoryFragment extends ListFragment {

    private HistoryViewModel historyViewModel;
    ListenerRegistration registration;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        historyViewModel =
                ViewModelProviders.of(this).get(HistoryViewModel.class);
        View root = inflater.inflate(R.layout.fragment_history, container, false);

        final TripListAdapter tripListAdapter = new TripListAdapter(getContext());
        setListAdapter(tripListAdapter);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();


        Query query = db.collection(currentUser.getEmail() + "-trips");
        registration = query.addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException e) {
                        tripListAdapter.clearTrips();

                        for (QueryDocumentSnapshot doc : value) {
                            Trip trip = doc.toObject(Trip.class);
                            tripListAdapter.addTrip(trip);
                        }
                    }
                });
        return root;
    }

    @Override
    public void onListItemClick(ListView l, View v, int pos, long id){
        Trip trip = (Trip) getListView().getItemAtPosition(pos);
        trip.setSerialized(true);
        startTripDetails(trip);
    }

    public void startTripDetails(Trip trip){
        Intent listIntent = new Intent(getActivity().getApplicationContext(), TripDetailsActivity.class);
        Gson gson = new Gson();
        listIntent.putExtra("trip", gson.toJson(trip));
        startActivity(listIntent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        registration.remove();
    }
}
