package com.teammerge.safetrip.uiFragment.history;

import android.os.AsyncTask;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.teammerge.safetrip.Trip;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class LoadTripData extends AsyncTask {
    TripListAdapter mAdapter;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    public LoadTripData(TripListAdapter adapter){
        this.mAdapter = adapter;
        this.mAuth = FirebaseAuth.getInstance();
        this.currentUser = mAuth.getCurrentUser();
    }
    @Override
    protected Object doInBackground(Object[] objects) {

        db.collection(currentUser.getEmail()+"-trips")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException e) {
                        for (QueryDocumentSnapshot doc : value) {
                            Trip trip = doc.toObject(Trip.class);
                            mAdapter.addTrip(trip);
                        }
                    }
                });
        return null;
        }
}
