package com.teammerge.safetrip.uiFragment.history;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.teammerge.safetrip.R;
import com.teammerge.safetrip.Trip;

import java.util.ArrayList;
import java.util.Collections;

public class TripListAdapter extends BaseAdapter {

    private Context mContext;

    private LayoutInflater mLayoutInflater;

    private ArrayList<Trip> mTrips = new ArrayList<Trip>();

    public TripListAdapter(Context context){
        this.mContext = context;
        mLayoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return mTrips.size();
    }

    @Override
    public Object getItem(int i) {
        return mTrips.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        FrameLayout itemView;
        if (view == null) {
            itemView = (FrameLayout) mLayoutInflater.inflate(R.layout.trip_list_item, viewGroup, false);

        } else {
            itemView = (FrameLayout) view;
        }
        Trip trip = mTrips.get(i);

        TextView titleText = (TextView) itemView.findViewById(R.id.lbl_tripSummary);
        TextView scoreText = (TextView) itemView.findViewById(R.id.lbl_score);
        TextView dateText = (TextView) itemView.findViewById(R.id.lbl_date);
        TextView durationText = (TextView) itemView.findViewById(R.id.lbl_tripDuration);
        TextView lengthText = (TextView) itemView.findViewById(R.id.lbl_tripLength);
        TextView pendingText = (TextView) itemView.findViewById(R.id.lbl_pending);

        if (trip.isApproved()){
            scoreText.setText(" "+Math.round(mTrips.get(i).getScore())+ mContext.getString(R.string.scorePoint));
            scoreText.setVisibility(View.VISIBLE);
            pendingText.setVisibility(View.GONE);
        } else {
            pendingText.setVisibility(View.VISIBLE);
            scoreText.setVisibility(View.GONE);
        }

        titleText.setText(getTripTitle(trip));
        dateText.setText(trip.getStartTime().toLocaleString());
        lengthText.setText(" " +(Math.round(trip.getLength()*10.0)/10.0) + " km");


        String duration = "";
        if (trip.getCalcDuration() > 60*60){
            int hour =(int) (trip.getCalcDuration()/(60*60));
            int minutes = (int) ((trip.getCalcDuration()%(60*60))/60);

            duration = " " + hour + mContext.getString(R.string.text_hour_symbol) + minutes +" min";
        }
        else {
            duration = " " + (int)(trip.getCalcDuration()/60) + " min";
        }

        durationText.setText(duration);



        return itemView;
    }

    public String getTripTitle(Trip trip){
        return mContext.getString(R.string.text_trip_from) + trip.getStartCity() + mContext.getString(R.string.text_to) + trip.getEndCity();
    }

    public void addTrip(Trip trip){
        this.mTrips.add(trip);
        Collections.sort(this.mTrips);
        Collections.reverse(this.mTrips);
        notifyDataSetChanged();
    }

    public void clearTrips() {
        this.mTrips = new ArrayList<>();
    }
}
