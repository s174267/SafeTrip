package com.teammerge.safetrip.uiFragment.home;

import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.SetOptions;
import com.shinelw.library.ColorArcProgressBar; // https://github.com/Shinelw/ColorArcProgressBar/blob/master/README.md

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.teammerge.safetrip.CarTrackingService;
import com.teammerge.safetrip.MainActivity;
import com.teammerge.safetrip.R;
import com.teammerge.safetrip.Trip;
import com.teammerge.safetrip.TripEvaluation;
import com.teammerge.safetrip.User;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private TextView greetingText;
    private Button startTrip;
    private Button endTrip;
    private ProgressBar progressBarUpload;
    private ColorArcProgressBar progressBarScore;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        greetingText = root.findViewById(R.id.lbl_greeting);
        startTrip = root.findViewById(R.id.btn_startTrip);
        endTrip = root.findViewById(R.id.btn_endTrip);
        progressBarUpload = root.findViewById(R.id.progressBarEndTrip);
        progressBarScore = root.findViewById(R.id.progressBarScore);
        final MainActivity activity = (MainActivity) getActivity();

        startTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startTrip(false);
                startTrip.setVisibility(View.INVISIBLE);
            }
        });

        endTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBarUpload.setVisibility(View.VISIBLE);
                endTrip.setEnabled(false);
                activity.endTrip();
            }
        });


        homeViewModel.getGreetingText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                greetingText.setText(s);
            }
        });


        homeViewModel.getScore().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer d) {
                progressBarScore.setCurrentValues(d);
            }
        });

        if(isMyServiceRunning(CarTrackingService.class)){
            Log.d("AUTO", "Service is Running");
            Log.d("AUTO END", endTrip.toString());
            endTrip.setText(R.string.end_trip);
            endTrip.setVisibility(View.VISIBLE);
            Log.d("AUTO START", startTrip.toString());
            startTrip.setVisibility(View.INVISIBLE);
            MainActivity.getInstance().tripStarted = true;
        }


        // Load the totalScore from firebase
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();


        db.collection("users").document(mAuth.getCurrentUser().getEmail())
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                User internalUser = document.toObject(User.class);
                                homeViewModel.setScore((int)internalUser.getTotalScore());
                                homeViewModel.setGreetingText(getString(R.string.text_hi) + internalUser.getDisplayName());
                            } else {
                                homeViewModel.setScore(0);
                                homeViewModel.setGreetingText(getString(R.string.text_hi));
                            }
                        } else {
                            Log.d("test_TAG", "get failed with ", task.getException());
                            homeViewModel.setScore(0);
                            homeViewModel.setGreetingText(getString(R.string.text_hi));
                        }
                    }
                });



        return root;


    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    @Override
    public void onPause() {
        Log.d("Home", "Pause");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.d("Home", "Stop");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.d("Home", "Destroy");
        super.onDestroyView();
    }



}
