package com.teammerge.safetrip.uiFragment.home;

import com.teammerge.safetrip.R;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class HomeViewModel extends ViewModel {

    private MutableLiveData<String> greetingText;
    private MutableLiveData<Integer> score;

    public HomeViewModel() {
        greetingText = new MutableLiveData<>();
        score = new MutableLiveData<>();
    }

    public void setGreetingText(String s) {
        greetingText.setValue(s);
    }





    public void setScore(Integer d) {
        score.setValue(d);
    }

    public LiveData<String> getGreetingText() {
        return greetingText;
    }

    public LiveData<Integer> getScore() {
        return score;
    }
}