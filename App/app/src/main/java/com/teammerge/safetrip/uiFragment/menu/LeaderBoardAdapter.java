package com.teammerge.safetrip.uiFragment.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.teammerge.safetrip.R;
import com.teammerge.safetrip.User;

import java.util.ArrayList;
import java.util.Locale;

public class LeaderBoardAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<User> items;

    public LeaderBoardAdapter(Context context, ArrayList<User> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.leaderboard_list_item, parent, false);
        }

        User currentItem = (User) getItem(position);

        TextView textName = (TextView) convertView.findViewById(R.id.lbl_name_leaderboard);
        TextView textScore = (TextView) convertView.findViewById(R.id.lbl_score_leaderboard);
        TextView textRank = (TextView) convertView.findViewById(R.id.lbl_name_rank);

        textName.setText(currentItem.getDisplayName());
        textScore.setText(String.format(Locale.forLanguageTag(Locale.getDefault().toLanguageTag()),"Score: %.0f", currentItem.getTotalScore()));
        textRank.setText( Integer.toString(position+1) );

        return convertView;
    }
}
