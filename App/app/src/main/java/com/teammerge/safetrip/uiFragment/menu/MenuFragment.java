package com.teammerge.safetrip.uiFragment.menu;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.firebase.auth.FirebaseAuth;
import com.teammerge.safetrip.AdminMenu;
import com.teammerge.safetrip.AuthenticationActivity;
import com.teammerge.safetrip.FaqActivity;
import com.teammerge.safetrip.LeaderBoardActivity;
import com.teammerge.safetrip.R;
import com.teammerge.safetrip.SettingsActivity;
import com.teammerge.safetrip.UserStatisticsActivity;

public class MenuFragment extends Fragment {
    private MenuViewModel menuViewModel;
    private ListView listView;

    private String items[] = new String[]{"Leaderboard", "FAQ", "Settings", "User Stats", "Log Out"};

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        menuViewModel = ViewModelProviders.of(this).get(MenuViewModel.class);
        View root = inflater.inflate(R.layout.fragment_menu, container, false);

        ImageView imgDarkSky = (ImageView) root.findViewById(R.id.imageDarkSky);
        imgDarkSky.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String url = "https://darksky.net/poweredby/";

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        final Context context = getActivity().getApplicationContext();
        ImageView imgDTUlogo = (ImageView) root.findViewById(R.id.imagedtulogo);
        imgDTUlogo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String url = "https://www.dtu.dk/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });


        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        if(mAuth.getCurrentUser().getEmail().equals("super-admin@mail.com") || mAuth.getCurrentUser().getEmail().equals("258@live.dk") || mAuth.getCurrentUser().getEmail().equals("niels.wm@gmail.com")){
            items = new String[]{"Leaderboard", "FAQ", "Settings", "User Stats", "Log Out", "Admin Menu"};
        }

        listView = root.findViewById(R.id.listView_menu);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, items);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0: // Leaderboard
                        Intent intentLeaderBoard = new Intent(getActivity().getApplicationContext(), LeaderBoardActivity.class);
                        startActivity(intentLeaderBoard);
                        break;

                    case 1: // FAQ
                        Intent intentFaq = new Intent(getActivity().getApplicationContext(), FaqActivity.class);
                        startActivity(intentFaq);
                        break;

                    case 2: // Settings
                        Intent intentSettings = new Intent(getActivity().getApplicationContext(), SettingsActivity.class);
                        startActivity(intentSettings);
                        break;

                    case 3: // User Stats
                        Intent intentUserStatistics = new Intent(getActivity().getApplicationContext(), UserStatisticsActivity.class);
                        startActivity(intentUserStatistics);
                        break;

                    case 4: // Log Out
                        FirebaseAuth mAuth = FirebaseAuth.getInstance();
                        mAuth.signOut();
                        Intent intentSignOut = new Intent(getActivity().getApplicationContext(), AuthenticationActivity.class);
                        intentSignOut.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intentSignOut);
                        break;

                    case 5: // Admin Menu
                        Intent adminMenu = new Intent(getActivity().getApplicationContext(), AdminMenu.class);
                        startActivity(adminMenu);
                        break;

                    default:
                        Toast.makeText(getContext().getApplicationContext(), items[position] + " does not contain anything", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        return root;
    }


}
