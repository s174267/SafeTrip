# SafeTrip - An App for Improving Driving Habits based on Smartphone Data

The SafeTrip Android application serves as the software product for our Bachelor of Science thesis at the Technical University of Denmark. The application serves as a platform for us to test our algorithms in a controlled environment.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You will need to have the Android Studio IDE installed. This is available for most common operating systems using the link below:

[https://developer.android.com/studio](https://developer.android.com/studio)

You can compile and run the application on either an emulator, on your computer or using an actual Android device. The minimum SDK version is API level 26, while the recommended is API level 29. 

Please note that for installing the application on an actual Android device through Android Studio you will need to have USB Debugging enabled in the Developer Options. Please see the link below for instructions on how to enable USB Debugging:

[https://developer.android.com/studio/debug/dev-options](https://developer.android.com/studio/debug/dev-options)


The application has been developed and tested using an Google Pixel XL, Samsung Galaxy S7 edge and Samsung Galaxy S8. 


### Install

There are two ways of installing the SafeTrip application. Either through the Android Studio IDE on a PC or Mac, or directly to your smartphone using a download link through Google Firebase App Distribution.

**Install using Android Studio**

1. Import project to Android Studio using the provided zip-file.
2. If using an actual Android device: Connect your Android device through USB.
3. Sync project with Gradle files.
4. Run the application on your desired device (Actual device or emulator).


**Install using Download Link**

1. Click the following link on your Android smartphone:
 [https://appdistribution.firebase.dev/i/zWB6TvG2](https://appdistribution.firebase.dev/i/zWB6TvG2)
 
 This will invite you to the limited deployment, enabling you to download and install the application directly on your smartphone.
 
2. Follow the on-screen instructions.



## Using the Application

This section will briefly explain how to use the SafeTrip application on first launch.

### First launch

Upon first launch, the application will prompt you to either sign-in or sign-up. The SafeTrip application requires access to location services and disk read/write.

#### I already have a user
If you already have a user, then input your email and password and click "Sign-In".

#### I do not have a user

If you do not have a user, click the "Sign Up Now" link at the bottom of the page. This will enable you to create a new user. Input your data and click "Sign up" when done.

#### Sign-in using our test user
If you do not want to input your credentials, you are free to use our test user. The credentials for logging into the test users are listed below: <br>
**Mail**: test@test.com <br>
**Password**: 12345678

## Running Tests
To run the automated tests run the following command in the command line interface:

```
./gradlew createDebugCoverageReport
```

The output file will then be located at:

```
/build/outputs/reports/coverage/debug/
```
The file is named:
```
index.html
```


## Authors

* **Victor Foged** - *s174267*
* **Niels With Mikkelsen** - *s174290*
* **Lasse Steen Pedersen** - *s174253*

### Link to our Git repository
Please use the following link to access our Gitlab repository:
```
https://gitlab.gbar.dtu.dk/s174267/SafeTrip
```
