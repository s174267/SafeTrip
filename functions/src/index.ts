import * as functions from 'firebase-functions';

const admin = require('firebase-admin');  
admin.initializeApp();
let db = admin.firestore()

class Trip {
    score: number;
    approved : boolean;
    calcDuration : number;
    constructor(score : number, approved : boolean, calcDuration : number){
        this.score = score;
        this.calcDuration = calcDuration;
        this.approved = approved;
    }

}
exports.scoreCalculations = functions.firestore
    .document('{trips}/{tripid}')
    .onWrite((change, context) => {
        let contextString = context.params.trips.toString();
        if(contextString == "users"){
            return "User Change";
        }
        let tripRef = db.collection(context.params.trips);
        let trips : Trip[] = [];
        return tripRef.orderBy('id', 'desc').limit(15).get().then((snapshot: { empty: any; forEach: (arg0: (doc: any) => void) => void; }) => {
            snapshot.forEach(doc => {
                trips.push(doc.data());
            });
            let sumScore = 0;
            let count = 0;
            let weightSum = 0;
            trips.forEach(trip => {
                if(trip.approved){
                    sumScore = sumScore + (trip.score * trip.calcDuration);
                    weightSum = weightSum + trip.calcDuration
                    count++;
                }
                if(count >= 10){
                    return;
                }
            });

            let userScore = Math.round(sumScore/weightSum);
            let string : string = context.params.trips.toString();
            let userEmail = string.substring(0, string.length - 6);

            let userRef = db.collection("users").doc(userEmail);
            return userRef.update({totalScore : userScore});

        })
        
         
    });